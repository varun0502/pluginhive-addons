<?php
/*
 * It will allow you to provide multiple weight and dimension to the variations of variable products.
 * Author : varun874
 */

if( ! class_exists('wf_Extend_Variable_Products') ) {
	class wf_Extend_Variable_Products{
		
		/**
		 * Constructor
		 */
		public function __construct() {
			add_action( 'woocommerce_variation_options_dimensions', array( $this, 'wf_add_more_dimentions_fields_variations'), 1000, 3 );
			add_action( 'woocommerce_process_product_meta_variable', array( $this, 'wf_save_additional_dimentions_fields_variable_product' ), 1, 1 );
		}
		
		/**
		 * Extends the settings of variation of variable products add multiple weight and dimension
		 * @param int $loop Number of variations
		 * @param array $variation_data Contains the variation data
		 * @param WP_Post $variation_object Variation object
		 */
		public function wf_add_more_dimentions_fields_variations( $loop, $variation_data, $variation_object ) {

			global $post;
			$thepostid = $variation_object->ID;
			if( empty($variation_object->ID) ) {
				WC_Admin_Meta_Boxes::add_error( __('Variation id could not found') );
			}
			
			$this->id	    = $variation_object->ID;
			$this->wc_product   = wc_get_product($this->id);
			$i = 2;
			if(!empty(get_post_meta( $thepostid, '_weight2', true ))) {
				$label_text="Fedex Pre-Pack 1";
			}
			else {
				$label_text="Fedex Pre-Pack";
			}
			?>
				<p class="form-field _pre_packed_field ">
					<label for="_pre_packed1"><?php echo $label_text; ?></label>
					<?php echo wc_help_tip( __( 'Check this if you want to ship this item as a individual package', 'woocommerce' ) ); ?>
					<input type="checkbox" class="checkbox" style="" name="_pre_packed_<?php echo $thepostid ?>_1" id="_pre_packed_<?php echo $thepostid ?>_1" value="yes" <?php echo get_post_meta( $thepostid, '_pre_packed1', true )=='yes'?'checked':''; ?>  />
				</p>
			<?php
				echo "<div id='wf_variations_dimensions".$thepostid."'>";
				while( $this->wf_get_meta_data("_weight$i", true) || $this->wf_get_meta_data("_length$i", true) || $this->wf_get_meta_data("_width$i", true) || $this->wf_get_meta_data("_height$i", true) )
				{
					?>
					<p class="form-field dimensions_field ">

							<label for="variation_weight2"><?php echo __( 'Weight ', 'woocommerce' ) .$i. ' (' . get_option( 'woocommerce_weight_unit' ) . ')'; ?></label>
							<input id="variation_weight2" placeholder="<?php wc_format_localized_decimal( 0 )?>" class="short wc_input_decimal" type="text" name="_dimension-<?php echo $thepostid."[$i]" ?>[weight]" value="<?php echo esc_attr( wc_format_localized_decimal( $this->wf_get_meta_data("_weight$i", true) ) ); ?>" />
					</p>
					<p class="form-field dimensions_field">
						<label for="variation_length2"><?php echo __( 'Dimensions ', 'woocommerce' ) .$i. ' (' . get_option( 'woocommerce_dimension_unit' ) . ')'; ?></label>
						<span class="wrap">
							<input id="variation_length2" placeholder="<?php echo esc_attr__( 'Length', 'woocommerce' ); ?>" class="input-text wc_input_decimal" size="6" type="text" name="_dimension-<?php echo $thepostid."[$i]" ?>[length]" value="<?php echo esc_attr( wc_format_localized_decimal( $this->wf_get_meta_data("_length$i", true) ) ); ?>" />
							<input placeholder="<?php echo esc_attr__( 'Width', 'woocommerce' ); ?>" class="input-text wc_input_decimal" size="6" type="text" name="_dimension-<?php echo $thepostid."[$i]" ?>[width]" value="<?php echo esc_attr( wc_format_localized_decimal( $this->wf_get_meta_data("_width$i", true) ) ); ?>" />
							<input placeholder="<?php echo esc_attr__( 'Height', 'woocommerce' ); ?>" class="input-text wc_input_decimal last" size="6" type="text" name="_dimension-<?php echo $thepostid."[$i]" ?>[height]" value="<?php echo esc_attr( wc_format_localized_decimal( $this->wf_get_meta_data("_height$i", true) ) ); ?>" />
						</span>

						<?php echo wc_help_tip( __( 'LxWxH in decimal form', 'woocommerce' ) ); ?>
					</p>
					<p class="form-field _pre_packed_field ">
						<label for="_pre_packed">Fedex Pre-Pack <?php echo $i;?></label>
						<span class="woocommerce-help-tip"></span>
						<input type="checkbox" class="checkbox" style="" name="_dimension-<?php echo $thepostid."[$i]" ?>[pre_packed]" value="yes"  <?php echo $this->wf_get_meta_data("_pre_packed$i", true)=='yes'?'checked':''; ?>>
					</p>

					<?php	
					$i++;			
				}
				echo '</div>';
				?>
				<button name="xa_add_more_fields_variation<?php echo $thepostid?>" type="button" class="button button-primary" id="xa_add_more_fields_variation<?php echo $thepostid?>">Add More Dimensions</button>
				<script>
				if( ! xa_product_addon ) {
					var xa_product_addon=<?php echo $i;?>;
				}
				else{
					xa_product_addon = xa_product_addon + <?php echo $i;?>;
				}

				jQuery('#xa_add_more_fields_variation<?php echo $thepostid?>').on('click',function()
				{
					var html_code='	<p class="form-field dimensions_field"><label for="variation_weight2"><?php echo __( 'Weight ', 'woocommerce' )."'+xa_product_addon+'" . ' (' . get_option( 'woocommerce_weight_unit' ) . ')'; ?></label><input id="product_weight'+xa_product_addon+'" placeholder="<?php echo wc_format_localized_decimal( 0 ); ?>" class="short wc_input_decimal" type="text" name="_dimension-<?php echo $thepostid ?>['+xa_product_addon+'][weight]" /></p>'+
					'<p class="form-field dimensions_field"><label for="variation_length2"><?php echo __( 'Dimensions ', 'woocommerce' )."'+xa_product_addon+'" . ' (' . get_option( 'woocommerce_dimension_unit' ) . ')'; ?></label><span class="wrap"><input  id="product_length"'+xa_product_addon+' placeholder="<?php esc_attr_e( 'Length ', 'woocommerce' ); ?>'+xa_product_addon+'" class="input-text wc_input_decimal" size="6" type="text" name="_dimension-<?php echo $thepostid ?>['+xa_product_addon+'][length]" /><input placeholder="<?php esc_attr_e( 'Width ', 'woocommerce' ); ?>'+xa_product_addon+'" class="input-text wc_input_decimal" size="6" type="text" name="_dimension-<?php echo $thepostid ?>['+xa_product_addon+'][width]" /><input placeholder="<?php esc_attr_e( 'Height ', 'woocommerce' ); ?>'+xa_product_addon+'" class="input-text wc_input_decimal last" size="6" type="text" name="_dimension-<?php echo $thepostid ?>['+xa_product_addon+'][height]"  />	</span><?php echo wc_help_tip( __( 'LxWxH in decimal form', 'woocommerce' ) ); ?></p>' +
					'<p class="form-field _pre_packed_field "><label for="_pre_packed">Fedex Pre-Pack '+xa_product_addon+'</label><span class="woocommerce-help-tip"></span><input type="checkbox" class="checkbox" style="" name="_dimension-<?php echo $thepostid ?>['+xa_product_addon+'][pre_packed]" id="_pre_packed'+xa_product_addon+'" value="yes" /> </p>';	
					jQuery('#wf_variations_dimensions<?php echo $thepostid?>').append(html_code);
					xa_product_addon++;
				});

				</script>
			<?php
		}
		
		/**
		 * Get post meta key
		 * @param string $meta_key Meta key to fetch
		 * @param boolean $single  true to get the single value and false to get array of the specified key
		 * @return mixed int | array | boolean
		 */
		public function wf_get_meta_data( $meta_key, $single = false ) {
			if( WC()->version < '3.0' ) {
				$meta_val = get_post_meta($this->id, $meta_key, $single);
			}
			else{
				$meta_val = $this->wc_product->get_meta($meta_key, $single);
			}
			return $meta_val;
		}
		
		
		/**
		 * Saves the data of variations of Variable product
		 * @param int $parent_id Parent product id
		 * @return null Doesn't return any data
		 */
		public function wf_save_additional_dimentions_fields_variable_product($parent_id) {

			$product = wc_get_product($parent_id);
			$child_product 	= $product->get_children();
			foreach ( $child_product as $child_product_id ) {
				// 1st dimension prepack option
				$pre_packed_1 	= ! empty($_POST['_pre_packed_'.$child_product_id.'_1']) ? $_POST['_pre_packed_'.$child_product_id.'_1'] : '';
				update_post_meta( $child_product_id, "_pre_packed1", $pre_packed_1 );
				// Remaining
				if( isset($_POST["_dimension-$child_product_id"]) ) {
					$i = 2;
					foreach( $_POST["_dimension-$child_product_id"] as $data ) {
						// Saves the data if length, width, height or weight is set
						if( ! empty($data['weight']) || ! empty($data['length']) || ! empty($data['width']) || ! empty($data['height']) ) {
							update_post_meta( $child_product_id, "_weight$i", $data['weight'] );
							update_post_meta( $child_product_id, "_length$i", $data['length'] );
							update_post_meta( $child_product_id, "_width$i", $data['width'] );
							update_post_meta( $child_product_id, "_height$i", $data['height'] );
							if( isset($data['pre_packed']) ) {
								update_post_meta( $child_product_id, "_pre_packed$i", $data['pre_packed'] );
							}
							else {
								update_post_meta( $child_product_id, "_pre_packed$i", '' );
							}
							$i++;
						}
						else {
							// Delete the data if length, width, height and weight is not set
							delete_post_meta($child_product_id, "_weight$i");
							delete_post_meta($child_product_id, "_length$i");
							delete_post_meta($child_product_id, "_width$i");
							delete_post_meta($child_product_id, "_height$i");
							delete_post_meta($child_product_id, "_pre_packed$i");
						}
					}
				}
			}
		}
	    }
	    new wf_Extend_Variable_Products();
}