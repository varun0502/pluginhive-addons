<?php

/*
 * To Support UPS and Fedex plugin.
 * Author : varun874
 */

if( ! class_exists('Xa_Fedex_Support_Product_Addon') ) {
	class Xa_Fedex_Support_Product_Addon{
		public function __construct() {
			// Support for Fedex Plugins
			add_filter( 'xa_alter_products_list', array( $this, 'wf_add_extra_packages' ) );
			// Support for UPS Plugins
			add_filter( 'xa_ups_alter_products_list', array( $this, 'wf_add_extra_packages' ) );
		}
		
		public function wf_add_extra_packages( $products ) {
			if( WC()->version < '3.0' ) {
				return $products;
			}

			$all_packages = null;
			foreach($products as $product ) {
				$pre_packed = get_post_meta( $product['data']->get_id() , '_wf_fedex_pre_packed_var', 1);
				if( empty( $pre_packed ) ){
					$pre_packed = get_post_meta( $product['data']->get_id() , '_wf_fedex_pre_packed', 1);
				}
				// Reset prepacked for first dimension if not set prepacked as overall( by plugin settings)
				if( $pre_packed != 'yes' ){
					$first_dimension_prepacked_option = $product['data']->obj->get_meta('_pre_packed1');
					if( $product['data'] instanceof wf_product ) {
						if( !empty($product['data']->obj) && $product['data']->obj instanceof WC_Product_Variation ) {
							$product['data']->obj->update_meta_data( '_wf_fedex_pre_packed_var', $first_dimension_prepacked_option);
						}
						elseif( ! empty($product['data']->obj) ) {
							$product['data']->obj->update_meta_data( '_wf_fedex_pre_packed', $first_dimension_prepacked_option);
						}
					}
					elseif( $product['data'] instanceof WC_Product_Variation ) {
						$product['data']->update_meta_data( '_wf_fedex_pre_packed_var', $first_dimension_prepacked_option);
					}
					else {
						$product['data']->update_meta_data( '_wf_fedex_pre_packed', $first_dimension_prepacked_option);
					}
				}
				$all_packages[]				= $product;

				$values				= $product;
				$values['data']		= clone $product['data'];	//overwriting the object else it will call the legacy class
				$success			= true;
				if( WC()->version < '3.0' && $values['data'] instanceof WC_Product_Variable) {
					return $products;
				}
				$this->id			= (WC()->version < '3.0' && ! ($values['data'] instanceof WC_Product_Simple ) ) ? ( ! empty($values['data']->get_variation_id()) ? $values['data']->get_variation_id() : $values['data']->get_id() ) : $values['data']->get_id();
				$this->wc_product		= is_object($values['data']->obj) ? $values['data']->obj : wc_get_product($this->id);
				$custom_field_index		= 2;

				while( $success ) {
				    $values['data']				= clone $product['data']; // Otherwise it will override the previous weight
					$values['data']->length		= $this->wf_get_meta_data("_length$custom_field_index", true);
					$values['data']->width		= $this->wf_get_meta_data("_width$custom_field_index", true);
					$values['data']->height		= $this->wf_get_meta_data("_height$custom_field_index", true);
					$values['data']->weight		= $this->wf_get_meta_data("_weight$custom_field_index", true);
					
					if( ! empty($values['data']->weight) ) {
						// Set the Weight and dimension in wc_product
						if( ! empty($values['data']->obj) && WC()->version > '2.7' ) {
							$xa_wc_product		= clone $values['data']->obj;
							$xa_wc_product->set_weight($values['data']->weight);
							$xa_wc_product->set_length($values['data']->length);
							$xa_wc_product->set_width($values['data']->width);
							$xa_wc_product->set_height($values['data']->height);
							if( $pre_packed != 'yes' ) {
								$pre_packed_for_additional_weight = $this->wf_get_meta_data("_pre_packed$custom_field_index", true);
								if( $xa_wc_product instanceof WC_Product_Variation ) {
									$xa_wc_product->update_meta_data( '_wf_fedex_pre_packed_var', $pre_packed_for_additional_weight );
								}
								else {
									$xa_wc_product->update_meta_data( '_wf_fedex_pre_packed', $pre_packed_for_additional_weight );
								}
							}
							$values['data']->obj = $xa_wc_product;
						}
						
						$all_packages = array_merge( $all_packages, array($values ) );
					}
					$custom_field_index++;
					$next_weight = $this->wf_get_meta_data("_weight$custom_field_index", true);
					if( empty($next_weight) ) {
						$success =false;
					}
				}
			}

			return $all_packages;
		}
		
		/**
		 * Get post meta key
		 * @param string $meta_key Meta key to fetch
		 * @param boolean $single  true to get the single value and false to get array of the specified key
		 * @return mixed int | array | boolean
		 */
		public function wf_get_meta_data( $meta_key, $single = false ) {
			if( WC()->version < '3.0' ) {
				$meta_val = get_post_meta($this->id, $meta_key, $single);
			}
			else{
				$meta_val = $this->wc_product->get_meta($meta_key, $single);
			}
			return $meta_val;
		}
		
		private function round_up( $value, $precision=2 ) { 
		    $pow = pow ( 10, $precision ); 
		    return ( ceil ( $pow * $value ) + ceil ( $pow * $value - ceil ( $pow * $value ) ) ) / $pow; 
		}
	}
	new Xa_Fedex_Support_Product_Addon();
}