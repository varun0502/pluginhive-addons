<?php
/**
 * Plugin Name: Add More Shipping Fields (For Multi Part Product)
 * Plugin URI: https://xadapter.com/
 * Description: This Plugin Will Add More Fields for Dimensions and Weight if your Product is a multiple part product.
 * Version: 1.0.8
 * Author: varun874
 * WC requires at least: 3.0
 * WC tested up to: 3.4
 * Requires at least: 4.4
 * Tested up to: 4.9
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_action('woocommerce_product_options_dimensions','wf_add_more_dimentions_fields');
add_action('woocommerce_process_product_meta_simple','wf_save_additional_dimentions_fields_simple_product',1);
// For variable products
require_once 'class-wf-extend-variable-products.php';
require_once 'class-xa-fedex-support-product-addon.php';

function wf_add_more_dimentions_fields()
{		global $post;

		$thepostid = $post->ID;
		$i=2;
		if(!empty(get_post_meta( $thepostid, '_weight2', true )))
		{
			$label_text="Fedex Pre-Pack 1";
		}
		else
		{
			$label_text="Fedex Pre-Pack";
		}
?>
			<p class="form-field _pre_packed_field ">
				<label for="_pre_packed1"><?php echo $label_text; ?></label>
				<?php echo wc_help_tip( __( 'Check this if you want to ship this item as a individual package', 'woocommerce' ) ); ?>
				<input type="checkbox" class="checkbox" style="" name="_pre_packed1" id="_pre_packed1" value="yes" <?php echo get_post_meta( $thepostid, '_pre_packed1', true )=='yes'?'checked':''; ?>  />
			</p>
<?php
		echo "<div id='wf_dimensions'>";
		while(metadata_exists( 'post',$thepostid, "_weight$i"))
		{
			?>
			<p class="form-field dimensions_field">
				
					<label for="product_weight2"><?php echo __( 'Weight ', 'woocommerce' ) .$i. ' (' . get_option( 'woocommerce_weight_unit' ) . ')'; ?></label>
					<input id="product_weight2" placeholder="<?php wc_format_localized_decimal( 0 )?>" class="short wc_input_decimal" type="text" name="_weight<?php echo $i.'"' ?>. value="<?php echo esc_attr( wc_format_localized_decimal( get_post_meta( $thepostid, '_weight'.$i, true ) ) ); ?>" />							
				
			</p>						
			<p class="form-field dimensions_field">
				<label for="product_length2"><?php echo __( 'Dimensions ', 'woocommerce' ) .$i. ' (' . get_option( 'woocommerce_dimension_unit' ) . ')'; ?></label>
				<span class="wrap">
					<input  id="product_length2" placeholder="<?php esc_attr_e( 'Length', 'woocommerce' ); ?>" class="input-text wc_input_decimal" size="6" type="text" name="_length<?php echo $i.'"' ?> value="<?php echo esc_attr( wc_format_localized_decimal( get_post_meta( $thepostid, '_length'.$i, true ) ) ); ?>" />
					<input placeholder="<?php esc_attr_e( 'Width', 'woocommerce' ); ?>" class="input-text wc_input_decimal" size="6" type="text" name="_width<?php echo $i.'"' ?> value="<?php echo esc_attr( wc_format_localized_decimal( get_post_meta( $thepostid, '_width'.$i, true ) ) ); ?>" />
					<input placeholder="<?php esc_attr_e( 'Height', 'woocommerce' ); ?>" class="input-text wc_input_decimal last" size="6" type="text" name="_height<?php echo $i.'"' ?> value="<?php echo esc_attr( wc_format_localized_decimal( get_post_meta( $thepostid, '_height'.$i, true ) ) ); ?>" />								
				</span>

				<?php echo wc_help_tip( __( 'LxWxH in decimal form', 'woocommerce' ) ); ?>
			</p>
			<p class="form-field _pre_packed_field ">
				<label for="_pre_packed">Fedex Pre-Pack <?php echo $i;?></label>
				<span class="woocommerce-help-tip"></span>
				<input type="checkbox" class="checkbox" style="" name="_pre_packed<?php echo  $i; ?>" id="_pre_packed<?php echo $i;?>" value="yes"  <?php echo get_post_meta( $thepostid, '_pre_packed'.$i, true )=='yes'?'checked':''; ?>>
			</p>
			
			<?php	
			$i++;			
		}
		
		?>
		</div>
		<p class="form-field dimensions_field">
		<button name="xa_add_more_fields" type="button" class="button button-primary" id="xa_add_more_fields">Add More Dimensions</button>
		</p>
	<script>
	var j=<?php echo $i;?>;

	jQuery('#xa_add_more_fields').on('click',function()
	{
			var html_code='	<p class="form-field dimensions_field"><label for="product_weight2"><?php echo __( 'Weight ', 'woocommerce' )."'+j+'" . ' (' . get_option( 'woocommerce_weight_unit' ) . ')'; ?></label><input id="product_weight'+j+'" placeholder="<?php echo wc_format_localized_decimal( 0 ); ?>" class="short wc_input_decimal" type="text" name="_weight'+j+'"/></p>'+
			'<p class="form-field dimensions_field"><label for="product_length2"><?php echo __( 'Dimensions ', 'woocommerce' )."'+j+'" . ' (' . get_option( 'woocommerce_dimension_unit' ) . ')'; ?></label><span class="wrap"><input  id="product_length"'+j+' placeholder="<?php esc_attr_e( 'Length ', 'woocommerce' ); ?>'+j+'" class="input-text wc_input_decimal" size="6" type="text" name="_length'+j+'" /><input placeholder="<?php esc_attr_e( 'Width ', 'woocommerce' ); ?>'+j+'" class="input-text wc_input_decimal" size="6" type="text" name="_width'+j+'" /><input placeholder="<?php esc_attr_e( 'Height ', 'woocommerce' ); ?>'+j+'" class="input-text wc_input_decimal last" size="6" type="text" name="_height'+j+'"  />	</span><?php echo wc_help_tip( __( 'LxWxH in decimal form', 'woocommerce' ) ); ?></p>' +
			'<p class="form-field _pre_packed_field "><label for="_pre_packed">Fedex Pre-Pack '+j+'</label><span class="woocommerce-help-tip"></span><input type="checkbox" class="checkbox" style="" name="_pre_packed'+j+'" id="_pre_packed'+j+'" value="yes" /> </p>';	
		jQuery('#wf_dimensions').append(html_code);
		j++;
	});
	
	</script>
		
		<?php

}
//146,048,513       ALTER IGNORE TABLE products ADD UNIQUE (asin);

function wf_save_additional_dimentions_fields_simple_product($post_id)
{
			$is_virtual      = isset( $_POST['_virtual'] ) ? 'yes' : 'no';
			if ( 'no' == $is_virtual ) {
			$key = $i=2;
			if ( isset( $_POST['_pre_packed1'] ) ) {
				update_post_meta( $post_id, '_pre_packed1', 'yes' );
			} else {
				update_post_meta( $post_id, '_pre_packed1', 'no' );
			}
			
			while( ( isset($_POST['_weight'.$i])  && isset($_POST['_length'.$i])  && isset($_POST['_width'.$i])  && isset($_POST['_height'.$i]) ) )
			{
				
				if( ! empty($_POST['_weight'.$i]) || ! empty($_POST['_length'.$i]) || ! empty($_POST['_width'.$i]) || ! empty($_POST['_height'.$i]) )
				{
					//for prepacked
					if ( isset( $_POST['_pre_packed'.$i] ) ) {
						update_post_meta( $post_id, '_pre_packed'.$key, 'yes' );
					} else {
						update_post_meta( $post_id, '_pre_packed'.$key, 'no' );
					}

					//for other fields
					if ( isset( $_POST['_weight'.$key] ) ) {
						update_post_meta( $post_id, '_weight'.$key, ( '' === $_POST['_weight'.$i] ) ? '' : wc_format_decimal( $_POST['_weight'.$i] ) );
					}

					if ( isset( $_POST['_length'.$key] ) ) {
						update_post_meta( $post_id, '_length'.$key, ( '' === $_POST['_length'.$i] ) ? '' : wc_format_decimal( $_POST['_length'.$i] ) );
					}

					if ( isset( $_POST['_width'.$key] ) ) {
						update_post_meta( $post_id, '_width'.$key, ( '' === $_POST['_width'.$i] ) ? '' : wc_format_decimal( $_POST['_width'.$i] ) );
					}

					if ( isset( $_POST['_height'.$key] ) ) {
						update_post_meta( $post_id, '_height'.$key, ( '' === $_POST['_height'.$i] ) ? '' : wc_format_decimal( $_POST['_height'.$i] ) );
					}
					
					$key++;
					
					$j = $key;
					while( ( $j <= $i ) && ( metadata_exists( 'post', $post_id, "_weight$i") ) )
					{
						delete_post_meta( $post_id, "_weight$i");
						delete_post_meta( $post_id, "_length$i");
						delete_post_meta( $post_id, "_width$i");
						delete_post_meta( $post_id, "_height$i");
						delete_post_meta( $post_id, "_pre_packed$i");
						$j++;
					}
				}
				else
				{
					$j = $i;
					while( ( metadata_exists( 'post', $post_id, "_weight$j") ) )
					{
						delete_post_meta( $post_id, "_weight$j");
						delete_post_meta( $post_id, "_length$j");
						delete_post_meta( $post_id, "_width$j");
						delete_post_meta( $post_id, "_height$j");
						delete_post_meta( $post_id, "_pre_packed$j");
						$j++;
					}
				}
				
				$i++;
			}


		} else {	
			update_post_meta( $post_id, '_weight', '' );
			update_post_meta( $post_id, '_length', '' );
			update_post_meta( $post_id, '_width', '' );
			update_post_meta( $post_id, '_height', '' );
		}
}