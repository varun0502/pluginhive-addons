<?php

/**
 * Plugin Name: Order Email Content Customizer
 * Plugin URI: https://pluginhive.com/
 * Description: Customize Email Content based on purchased Items in the Order
 * Version: 1.0.0
 * Author: Varun
 * Author URI: https://pluginhive.com
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if( ! class_exists('Ph_Order_Email_Content_Customizer') ) {
	require_once 'includes/class-ph-order-email-content-customizer.php';
}

// Provide Product Id / Variation Id
$product = array(
	'ups'				=>	array( 3334, 3335, 3336, 3337 ),
	'fedex'				=>	array( 3320, 3321, 3322, 3323 ),
	'bookings'			=>	array( 624, 1276, 1277, 1278 ),
	'table_rate'		=>	array( 3289, 79420, 79421, 79422 ),
	'canada_post'		=>	array( 3290, 3292, 3293, 3294 ),
	'shipment_tracking'	=>	array( 3240, 3286, 3287, 3288 ),
	'multi_carrier'		=>	array( 3279, 3283, 3284, 3285 ),
	'royal_mail'		=>	array( 285, 797, 798, 799 ),
	'est_delivery'		=>	array( 3248, 3269, 3270, 3271 ),
	'stamps_magento'	=>	array( 78568 ),
	'singapore_post'	=>	array( 411),
);
$object = Ph_Order_Email_Content_Customizer::instance();
$object->init( $product );