<?php

if( ! defined('ABSPATH') )	exit;

if( ! class_exists('Ph_Product_Email_Contents') ) {
	class Ph_Product_Email_Contents {
		public static function print_email_content( $product_name = null ) {
			switch($product_name) {
				case 'ups'				: echo "Please refer to the following documentation to get started with the UPS Shipping Plugin with Print Label for WooCommerce.<br>
										<a href='https://www.pluginhive.com/knowledge-base/how-to-download-install-update-woocommerce-plugin/?seq_no=2'>Installation</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/setting-woocommerce-ups-shipping-plugin/'>Setting up</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/troubleshooting-woocommerce-ups-plugin/'>Troubleshooting</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/category/woocommerce-ups-shipping-plugin-with-print-label/'>Knowledge base</a><br>
										";
										break;
				case 'fedex'			: echo "Please refer to the following documentation to get started with the FedEx Shipping Plugin with Print Label for WooCommerce.<br>
										<a href='https://www.pluginhive.com/knowledge-base/how-to-download-install-update-woocommerce-plugin/?seq_no=2'>Installation</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/setting-woocommerce-fedex-shipping-plugin/'>Setting up</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/troubleshooting-woocommerce-fedex-shipping-plugin/'>Troubleshooting</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/category/woocommerce-fedex-shipping-plugin-with-print-label/'>Knowledge base</a><br>
										";
										break;
				case 'bookings'			: echo "Please refer to the following documentation to get started with the Bookings and Appointments.<br>
										<a href='https://www.pluginhive.com/knowledge-base/how-to-download-install-update-woocommerce-plugin/?seq_no=2'>Installation</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/setting-woocommerce-bookings-appointments-plugin/'>Setting up</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/category/bookings-and-appointments/'>Knowledge base</a><br>
										";
										break;
				case 'table_rate'		: echo "Please refer to the following documentation to get started with the Table Rate Shipping Pro Plugin for WooCommerce.<br>
										<a href='https://www.pluginhive.com/knowledge-base/how-to-download-install-update-woocommerce-plugin/?seq_no=2'>Installation</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/woocommerce-shipping-pro-chapter-1-setting-plugin/'>Setting up</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/category/woocommerce-table-rate-shipping-pro-plugin/'>Knowledge base</a><br>
										";
										break;
				case 'canada_post'		: echo "Please refer to the following documentation to get started with the Canada Post Shipping Plugin with Print Label for WooCommerce.<br>
										<a href='https://www.pluginhive.com/knowledge-base/how-to-download-install-update-woocommerce-plugin/?seq_no=2'>Installation</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/setting-woocommerce-canada-post-shipping-plugin/'>Setting up</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/troubleshooting-woocommerce-canada-post-plugin/'>Troubleshooting</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/category/woocommerce-canada-post-shipping-plugin-with-print-label/'>Knowledge base</a><br>
										";
										break;
				case 'shipment_tracking': echo "Please refer to the following documentation to get started with the Shipment Tracking Pro for WooCommerce.<br>
										<a href='https://www.pluginhive.com/knowledge-base/how-to-download-install-update-woocommerce-plugin/?seq_no=2'>Installation</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/setting-woocommerce-shipment-tracking-pro-plugin/'>Setting up</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/category/woocommerce-shipment-tracking-pro/'>Knowledge base</a><br>
										";
										break;
				case 'multi_carrier'	: echo "Please refer to the following documentation to get started with the Multiple Carrier Shipping Plugin for WooCommerce.<br>
										<a href='https://www.pluginhive.com/knowledge-base/how-to-download-install-update-woocommerce-plugin/?seq_no=2'>Installation</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/setting-multi-carrier-shipping-plugin-woocommerce/'>Setting up</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/troubleshoot-multi-carrier-shipping-plugin-woocommerce/'>Troubleshooting</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/category/multiple-carrier-shipping-plugin-for-woocommerce/'>Knowledge base</a><br>
										";
										break;
				case 'royal_mail'		: echo "Please refer to the following documentation to get started with the WooCommerce Royal Mail Shipping with Tracking.<br>
										<a href='https://www.pluginhive.com/knowledge-base/how-to-download-install-update-woocommerce-plugin/?seq_no=2'>Installation</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/setting-woocommerce-royal-mail-shipping-tracking-plugin/'>Setting up</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/troubleshooting-woocommerce-royal-mail-shipping-tracking-plugin/'>Troubleshooting</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/category/royal-mail-shipping-plugin/'>Knowledge base</a><br>
										";
										break;
				case 'est_delivery'		: echo "Please refer to the following documentation to get started with the Estimated Delivery Date Plugin For WooCommerce.<br>
										<a href='https://www.pluginhive.com/knowledge-base/how-to-download-install-update-woocommerce-plugin/?seq_no=2'>Installation</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/setting-estimated-delivery-date-plugin-woocommerce/'>Setting up</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/category/estimated-delivery-date-plugin-for-woocommerce/'>Knowledge base</a><br>
										";
										break;
				case 'stamps_magento'	: echo "Please refer to the following documentation to get started with the UPS plugin.<br>
										<a href='https://www.pluginhive.com/knowledge-base/how-to-download-install-update-magento-2-0-extension/'>Installation</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/setting-up-magento-stamps-usps-shipping-plugin/'>Setting up</a><br>
										<a href='https://www.pluginhive.com/knowledge-base/category/stamps-com-usps-shipping-extension-with-postage-for-magento/'>Knowledge base</a><br>
										";
										break;
				case 'singapore_post'	: echo " ";
										break;
				default					: break;
			}
		}
	}
}