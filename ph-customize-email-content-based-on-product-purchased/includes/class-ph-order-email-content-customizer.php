<?php

if( ! class_exists('Ph_Order_Email_Content_Customizer') ) {
	class Ph_Order_Email_Content_Customizer {

		/**
		 * The single instance of the class.
		 */
		protected static $_instance = null;

		/**
		 * Main Ph_Order_Email_Content_Customizer Instance.
		 * Ensures only one instance of Ph_Order_Email_Content_Customizer is loaded or can be loaded.
		 * @return Ph_Order_Email_Content_Customizer - Main instance.
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self();
			}
			return self::$_instance;
		}
		
		/**
		 * Initialize.
		 */
		public function init( $product_details = array() ) {
			$this->product_details = $product_details;
			add_action( 'woocommerce_email_customer_details', array( $this, 'add_content_to_email'), 100, 2 );
		}

		/**
		 * Add Content to the Email.
		 * @param WC_Order $order
		 * @param boolean $sent_to_admin
		 */
		public function add_content_to_email( $order, $sent_to_admin ) {
			// Return if it is sent to Admin or Order is not a instance of WC_Order
			if( $sent_to_admin || ! is_a( $order, 'WC_Order') )
				return;
			$order_status = $order->get_status();		// Order Status
			if( $order_status != 'completed' ) {
				return;
			}
			$order_items = $order->get_items();

			foreach( $order_items as $order_item ) {
				if( is_a( $order_item, 'WC_Order_Item_Product' ) ) {
					$product_id = $order_item->get_variation_id();
					if( empty($product_id) ) {
						$product_id = $order_item->get_product_id();
					}

					foreach( $this->product_details as $plugin_name => $product_id_arr ) {
						if( in_array( $product_id, $product_id_arr ) )
							$product_content[] = $plugin_name;
					}
				}
			}
			if( ! empty($product_content) ) {
				$this->add_content_based_on_product($product_content);
			}
		}

		public function add_content_based_on_product( $product_content = array() ) {
			array_unique( $product_content );
			if( ! empty($product_content) ) {
				echo '<h3>Documentation</h3>';
				if( ! class_exists('Ph_Product_Email_Contents') )
					require_once 'class-ph-product-email-contents.php';
				foreach ( $product_content as $product_name ) {
					Ph_Product_Email_Contents::print_email_content($product_name);
				}
				echo "If you need additional help, please contact our <a href='https://www.pluginhive.com/support/'>Support team</a>.<br>";
			}
		}
	}
}