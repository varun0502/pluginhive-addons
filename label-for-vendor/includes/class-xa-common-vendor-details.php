<?php

if( ! class_exists('Xa_Common_vendor_details') ) {

	class Xa_Common_vendor_details {

		/**
		 * Get the Vendor Address .
		 */
		public function get_vendor_address( $vndr_id ){
			$vendor_profile = get_user_meta($vndr_id);

			$vendor_details= array();
			switch (VENDOR_PLUGIN) {
				case 'dokan_lite':

					if( function_exists('dokan_get_seller_id_by_order') ){
						$dokan_profile = get_user_meta( $vndr_id, 'dokan_profile_settings', true );
					}

					//For older version of Dokan plugin.
					if( empty($dokan_profile['address']) ){
						$dokan_profile = isset( $vendor_profile['dokan_profile_settings'][0] ) ? unserialize( $vendor_profile['dokan_profile_settings'][0] ) : '';
					}
					
					$vendor_details['vendor_country'] 	= isset( $dokan_profile['address']['country'] ) ? $dokan_profile['address']['country'] : '';
					$vendor_details['vendor_fname']		= isset( $vendor_profile['billing_first_name'][0] ) ? $vendor_profile['billing_first_name'][0] : '' ;
					$vendor_details['vendor_lname']		= isset( $vendor_profile['billing_last_name'][0] ) ? $vendor_profile['billing_last_name'][0] : '';
					$vendor_details['vendor_company']	= isset( $dokan_profile['store_name'] ) ? $dokan_profile['store_name'] : '';
					$vendor_details['vendor_address1']	= isset( $dokan_profile['address']['street_1'] ) ? $dokan_profile['address']['street_1'] : '';
					$vendor_details['vendor_address2']	= isset( $dokan_profile['address']['street_2'] ) ? $dokan_profile['address']['street_2'] : '';
					$vendor_details['vendor_city']		= isset( $dokan_profile['address']['city'] ) ? $dokan_profile['address']['city'] : '';
					$vendor_details['vendor_state']		= isset( $dokan_profile['address']['state'] ) ? $dokan_profile['address']['state'] : '';
					$vendor_details['vendor_zip']		= isset( $dokan_profile['address']['zip'] ) ? $dokan_profile['address']['zip'] : '';
					$vendor_details['vendor_phone']		= isset( $dokan_profile['phone'] ) ? $dokan_profile['phone'] : '';
					$vendor_details['email']			= isset( $vendor_profile['billing_email'][0] ) ? $vendor_profile['billing_email'][0] : '';
					break;

				case 'wc_vendors_pro':
					$vendor_details['vendor_country'] 	= isset( $vendor_profile['_wcv_store_country'][0] ) ? $vendor_profile['_wcv_store_country'][0] : '';
					$vendor_details['vendor_fname']		= isset( $vendor_profile['first_name'][0] ) ? $vendor_profile['first_name'][0] : '';
					$vendor_details['vendor_lname']		= isset( $vendor_profile['last_name'][0] ) ? $vendor_profile['last_name'][0] : '';
					$vendor_details['vendor_company']	= isset( $vendor_profile['pv_shop_name'][0] ) ? $vendor_profile['pv_shop_name'][0] : '';
					$vendor_details['vendor_address1']	= isset( $vendor_profile['_wcv_store_address1'][0] ) ? $vendor_profile['_wcv_store_address1'][0] : '';
					$vendor_details['vendor_address2']	= isset( $vendor_profile['_wcv_store_address2'][0] ) ? $vendor_profile['_wcv_store_address2'][0] : '';
					$vendor_details['vendor_city']		= isset( $vendor_profile['_wcv_store_city'][0] ) ? $vendor_profile['_wcv_store_city'][0] : '';
					$vendor_details['vendor_state']		= isset( $vendor_profile['_wcv_store_state'][0] ) ? $vendor_profile['_wcv_store_state'][0] : '';
					$vendor_details['vendor_zip']		= isset( $vendor_profile['_wcv_store_postcode'][0] ) ? $vendor_profile['_wcv_store_postcode'][0] : '';
					$vendor_details['vendor_phone']		= isset( $vendor_profile['_wcv_store_phone'][0] ) ? $vendor_profile['_wcv_store_phone'][0] : '';
					$vendor_details['email']			= isset( $vendor_profile['billing_email'][0] ) ? $vendor_profile['billing_email'][0] : '';
					break;
				
				default:

					$vendor_details['vendor_country'] 	= isset( $vendor_profile['billing_country'][0] ) ? $vendor_profile['billing_country'][0] : '';
					$vendor_details['vendor_fname']		= isset( $vendor_profile['billing_first_name'][0] ) ? $vendor_profile['billing_first_name'][0] : '';
					$vendor_details['vendor_lname']		= isset( $vendor_profile['billing_last_name'][0] ) ? $vendor_profile['billing_last_name'][0] : '';
					$vendor_details['vendor_company']	= isset( $vendor_profile['billing_company'][0] ) ? $vendor_profile['billing_company'][0] : '';
					$vendor_details['vendor_address1']	= isset( $vendor_profile['billing_address_1'][0] ) ? $vendor_profile['billing_address_1'][0] : '';
					$vendor_details['vendor_address2']	= isset( $vendor_profile['billing_address_2'][0] ) ? $vendor_profile['billing_address_2'][0] : '';
					$vendor_details['vendor_city']		= isset( $vendor_profile['billing_city'][0] ) ? $vendor_profile['billing_city'][0] : '';
					$vendor_details['vendor_state']		= isset( $vendor_profile['billing_state'][0] ) ? $vendor_profile['billing_state'][0] : '';
					$vendor_details['vendor_zip']		= isset( $vendor_profile['billing_postcode'][0] ) ? $vendor_profile['billing_postcode'][0] : '';
					$vendor_details['vendor_phone']		= isset( $vendor_profile['billing_phone'][0] ) ? $vendor_profile['billing_phone'][0] : '';
					$vendor_details['email']			= isset( $vendor_profile['billing_email'][0] ) ? $vendor_profile['billing_email'][0] : '';
					break;
			}
			$vendor_details['tin_number']			= isset( $vendor_profile['tin_number'][0] ) ? $vendor_profile['tin_number'][0] : '';
			return $vendor_details;
		}

		/**
		 * Get the Formatted address.
		 */
		public function wf_formate_origin_address($vendor_address){
			return array(
				'country' 		=> $vendor_address['vendor_country'],
				'first_name'	=> $vendor_address['vendor_fname'],
				'last_name'		=> $vendor_address['vendor_lname'],
				'company'		=> $vendor_address['vendor_company'],
				'address_1'		=> $vendor_address['vendor_address1'],
				'address_2'		=> $vendor_address['vendor_address2'],
				'city' 			=> $vendor_address['vendor_city'],
				'state'			=> $vendor_address['vendor_state'],
				'postcode' 		=> $vendor_address['vendor_zip'],
				'phone' 		=> $vendor_address['vendor_phone'],
				'email' 		=> $vendor_address['email'],
				'tin_number' 	=> isset($vendor_address['tin_number']) ? $vendor_address['tin_number'] : '',

			);
		}
	}
}