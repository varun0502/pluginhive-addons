<?php

global $xa_current_user;
$url 						= get_permalink( get_option('woocommerce_myaccount_page_id') );
$order_id 					= $_GET['xa_view_order_on_front_end'];
$order 						= wc_get_order($order_id);
$product_belongs_to_author 	= false;

if( $order instanceof WC_Order ) {

	$order_date_created 	= date_create($order->get_date_created());
	$date_format 			= get_option('date_format');
	$create_fedex_label		= base64_encode($order_id .'|'. $xa_current_user->ID);
	$create_ups_label		= base64_encode($order_id.'|'. $xa_current_user->ID);

	?>
		<!-- To define JQuery -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"> </script>
		<script type="text/javascript">
			// Go back to all order page
			function go_back_to_all_order_page() {
				location.href = +'';
			}
		</script>
		<style>
			* {
			    box-sizing: border-box;
			}
			/* Create two equal columns that floats next to each other */
			.xa_divided_parts_of_the_page {
			    float: left;
			    width: 33%;
			    padding: 10px;
			}

			/* Clear floats after the columns */
			.xa_divide_page_into_multiple_parts:after {
			    content: "";
			    display: table;
			    clear: both;
			}

			.xa-go-back-button-on-order-page {
				width: 200px;
				padding: 20px;
				cursor: pointer;
				box-shadow: 6px 6px 5px; #999;
				-webkit-box-shadow: 6px 6px 5px #999;
				-moz-box-shadow: 6px 6px 5px #999;
				font-weight: bold;
				background: blue;
				color: white;
				border-radius: 10px;
				border: 1px solid #999;
				font-size: 150%;
			}

			.xa-label-button-on-order-page {
				/* width: 180px; */
				padding: 5px;
				cursor: pointer;
				box-shadow: 6px 6px 5px; #999;
				-webkit-box-shadow: 6px 6px 5px #999;
				-moz-box-shadow: 6px 6px 5px #999;
				font-weight: bold;
				background: #404BBD;
				color: white;
				border-radius: 10px;
				font-size: 150%;
			}
		</style>

		<input type="button" value="Go Back" class="xa-go-back-button-on-order-page" onclick="go_back_to_all_order_page()" />
	<?php

	echo "<h2> Order #".$order->get_order_number()." details </h2>";	// Start

	echo '<div class = "xa_divide_page_into_multiple_parts">';
		// General details about order
		echo '<div class = "xa_divided_parts_of_the_page">';
			echo '<h4>General </h4>';
				echo '<table style = "text-align : left;">';
					echo '<tr>';
						echo '<th>Date Created :</th>';
						echo '<td>'.$order_date_created->format($date_format.' H:i:s').'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Status :</th>';
						echo '<td>'.$order->get_status().'</td>';
					echo '</tr>';
				echo '</table>';
		echo '</div>';

		// Address Details
		$billing_address 	= $order->get_address('billing');
		$shipping_address 	= $order->get_address('shipping');

		// Billing Address
		echo '<div class = "xa_divided_parts_of_the_page">';
			echo '<h4>Billing Address </h4>';
				echo '<table style = "text-align : left;">';
					echo '<tr>';
						echo '<th>First Name :</th>';
						echo '<td>'.$billing_address['first_name'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Last Name :</th>';
						echo '<td>'.$billing_address['last_name'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Company :</th>';
						echo '<td>'.$billing_address['company'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Address line 1 :</th>';
						echo '<td>'.$billing_address['address_1'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Address line 2:</th>';
						echo '<td>'.$billing_address['address_2'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>City:</th>';
						echo '<td>'.$billing_address['city'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>State / County:</th>';
						echo '<td>'.$billing_address['state'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Postcode / ZIP:</th>';
						echo '<td>'.$billing_address['postcode'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Country :</th>';
						echo '<td>'.$billing_address['country'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Email address :</th>';
						echo '<td>'.$billing_address['email'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Phone :</th>';
						echo '<td>'.$billing_address['phone'].'</td>';
					echo '</tr>';
				echo '</table>';
			
		echo '</div>';

		// Shipping Address
		echo '<div class = "xa_divided_parts_of_the_page">';
			echo '<h4>Shipping Address </h4>';
				echo '<table style = "text-align : left;">';
					echo '<tr>';
						echo '<th>First Name :</th>';
						echo '<td>'.$shipping_address['first_name'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Last Name :</th>';
						echo '<td>'.$shipping_address['last_name'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Company :</th>';
						echo '<td>'.$shipping_address['company'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Address line 1 :</th>';
						echo '<td>'.$shipping_address['address_1'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Address line 2:</th>';
						echo '<td>'.$shipping_address['address_2'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>City:</th>';
						echo '<td>'.$shipping_address['city'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>State / County:</th>';
						echo '<td>'.$shipping_address['state'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Postcode / ZIP:</th>';
						echo '<td>'.$shipping_address['postcode'].'</td>';
					echo '</tr>';
					echo '<tr>';
						echo '<th>Country :</th>';
						echo '<td>'.$shipping_address['country'].'</td>';
					echo '</tr>';
				echo '</table>';
		echo '</div>';
	echo '</div>';

	// Order product details associated with this vendor
	$order_items = $order->get_items();
	echo "<br/><div>";
		echo '<h3>Product Details :</h3>';
		echo '<table style = "text-align : left; width :80%; margin-left :5%;">';
			echo '<tr>';
				echo '<th>Item</th>';
				echo "<th>Cost</th>";
				echo "<th>Qty</th>";
				echo "<th>Total</th>";
			echo '</tr>';
			foreach( $order_items as $line_item ) {
				$product 	= $line_item->get_product();
				$post 		= get_post($product->get_id());
				if( $post->post_author == $xa_current_user->ID )
				{
					$product_belongs_to_author = true;
					echo '<tr>';
						echo "<td>".$line_item->get_name()."</td>";
						echo "<td>".$product->get_price()."</td>";
						echo "<td>".$line_item->get_quantity()."</td>";
						echo "<td>".$line_item->get_total()."</td>";
					echo '</tr>';
				}
			}
		echo '</table>';
	echo "</div>";


	echo '<br/><div class = "xa_divide_page_into_multiple_parts">';

		// Fedex Label Details
		echo '<div class = "xa_divided_parts_of_the_page">';
			echo '<h4>FedEx </h4>';
				$shipment_ids = get_post_meta( $order_id, 'wf_vendor_'.$xa_current_user->ID.'_woo_fedex_shipmentId' );
				if( empty($shipment_ids)  && $product_belongs_to_author ) {

					$fedex_services = include 'fedex/data-wf-service-codes.php';
					echo "<select name='xa_create_vendor_fedex_label_service' class ='xa_create_vendor_fedex_label_service'>";
						foreach( $fedex_services as $fedex_service => $fedex_service_name ) {
							echo "<option value='$fedex_service'>$fedex_service_name</option>";
						}
					echo "</select><br/><br/>";
					?>

						<script type="text/javascript">
							function xa_create_vendor_fedex_label() {
								let selected_service =jQuery(".xa_create_vendor_fedex_label_service option:selected").val();
								let create_fedex_label = "<?php echo $order_id ?>";
								location.href = '?&wf_fedex_createshipment='+create_fedex_label+'&create_shipment_service='+selected_service;
								return false;
								};
						</script>

						<input type="button" value="Create Label" class="xa-label-button-on-order-page xa_create_vendor_fedex_label"  onclick="xa_create_vendor_fedex_label()" />
					<?php
				}
				else{
					?>
						<script>
							function xa_create_vendor_fedex_return_label(shipment_id){
								let selected_service =jQuery(".xa_create_vendor_fedex_label_service option:selected").val();
								let create_fedex_label = "<?php echo $order_id ?>";
								location.href = '?&wf_create_return_label='+create_fedex_label+'&create_shipment_service='+selected_service+'&ph_fedex_shipment_id='+shipment_id;
								return false;
							}
						</script>
					<?php
					$fedex_services = include 'fedex/data-wf-service-codes.php';
					echo '<table>';
						echo '<tr>';
							echo '<th>Shipment Id</th>';
							echo '<th>Action</th>';
						echo '</tr>';
						foreach( $shipment_ids as $shipment_id ) {
							$print_fedex_label = $url.'?xa_print_vendor_fedex_label='.base64_encode($order_id .'|'.$xa_current_user->ID.'|'.$shipment_id);
							?>
							<tr>
								<td>
									<?php echo $shipment_id; ?>
								</td>
								<td>
									<input type="button" value="Print Label" class="xa-label-button-on-order-page" onclick="window.location.href='<?php echo $print_fedex_label; ?>'" />
								</td>
							</tr>
							<?php
								$return_shipment_id = get_post_meta( $order_id, 'wf_vendor_'.$xa_current_user->ID.'_woo_fedex_returnShipmetId'.$shipment_id, true);
								if( ! empty($return_shipment_id) ){
									$print_fedex_label = $url.'?xa_print_vendor_fedex_return_label='.base64_encode($order_id .'|'.$xa_current_user->ID.'|'.$return_shipment_id);
									echo "<tr><td> $return_shipment_id</td>";
									?>
									<td>
										<input type="button" value="Print Return Label" class="xa-label-button-on-order-page" onclick="window.location.href='<?php echo $print_fedex_label; ?>'" />
									</td>
									<?php
									echo '</tr>';
								}
								else{
									echo '<tr><td>';
									foreach( $shipment_ids as $shipment_id ) {
										echo "<select name='xa_create_vendor_fedex_label_service' class ='xa_create_vendor_fedex_label_service'>";
										foreach( $fedex_services as $fedex_service => $fedex_service_name ) {
											echo "<option value='$fedex_service'>$fedex_service_name</option>";
										}
										echo "</select></td><td>";
										echo '<input type="button" value="Create Return Label" class="xa-label-button-on-order-page xa_create_vendor_fedex_return_label"  onclick="xa_create_vendor_fedex_return_label('.$shipment_id.')" />';
										echo '</td></tr>';
								}
							}
							?>
							<?php
						}
					echo '</table>';
				}
		echo '</div>';


		// UPS Label Details
		/*echo '<div class = "xa_divided_parts_of_the_page">';
			echo '<h4>UPS </h4>';
				if(0) {
					?>
						<input type="button" value="Create Label" class="xa-label-button-on-order-page" onclick="window.location.href='<?php echo $create_ups_label; ?>'" />
					<?php
				}
				else{
					?>
						<input type="button" value="Print Label" class="xa-label-button-on-order-page" onclick="window.location.href='<?php echo $print_ups_label; ?>'" />
					<?php
				}
		echo '</div>';*/
	echo "</div>";
	echo '<br/><br/><br/>';
}
?>

<input type="button" value="Go Back" class="xa-go-back-button-on-order-page" onclick="go_back_to_all_order_page()" />