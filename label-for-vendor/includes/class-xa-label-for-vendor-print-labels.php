<?php


if( ! class_exists('Xa_Label_For_Vendor_Print_Labels') ) {
	class Xa_Label_For_Vendor_Print_Labels {

		public function __construct(){

			global $xa_active_plugins;

			add_action( 'woocommerce_after_register_post_type', array( $this, 'check_for_create_shipment_action' ) );
			
			add_action( 'init', array( $this, 'my_custom_endpoints' ) );
			add_filter( 'query_vars', array( $this, 'my_custom_query_vars'), 0 );
			add_action( 'wp_loaded', array( $this, 'my_custom_flush_rewrite_rules' ));
			add_action( 'woocommerce_account_xa-all-order_endpoint', array( $this, 'my_custom_endpoint_content') );
			add_filter( 'woocommerce_account_menu_items', array( $this, 'add_navigation_menu') );
		}

		public function check_for_create_shipment_action() {

			global $xa_current_user;
			$xa_current_user = wp_get_current_user();

			// View any specific order on dashboard
			if( ! empty($_GET['xa_view_order_on_front_end']) ) {
				$order_id 	= $_GET['xa_view_order_on_front_end'];
				include 'html-xa-vendor-view-order.php';
				die;
			}
			// // If create label button has been clicked for FedEx or UPS
			// elseif( ! empty($_GET['xa_create_vendor_fedex_label']) || ! empty($_GET['xa_create_vendor_ups_label']) ) {
			// 	$this->xa_create_shipment_label();
			// }
			// To print Label
			elseif( ! empty($_GET['xa_print_vendor_fedex_label']) || ! empty($_GET['xa_print_vendor_ups_label']) ) {
				$this->print_labels();
			}
			elseif( ! empty($_GET['xa_print_vendor_fedex_return_label']) ) {
				$this->print_return_label();
			}
			elseif( ! empty($_GET['wf_fedex_createshipment']) || ! empty($_GET['wf_create_return_label']) ) {

				if( ! class_exists('Xa_Label_For_Vendor_Fedex_Support') || ! empty($_GET['wf_create_return_label']) ) {
					require_once 'fedex/class-xa-label-for-vendor-fedex-support.php';
				}
				$fedex_support = new Xa_Label_For_Vendor_Fedex_Support();
			}

		}

		public function xa_create_shipment_label(){
			
			global $xa_current_user;

			// Create Fedex Label
			if( ! empty($_GET['xa_create_vendor_fedex_label']) ) {
				list( $order_id, $user_id ) = explode( '|', base64_decode($_GET['xa_create_vendor_fedex_label']) );
				$order 						= wc_get_order($order_id);

				if( ! class_exists('Xa_Label_For_Vendor_Fedex_Support') ) {
					require_once 'fedex/class-xa-label-for-vendor-fedex-support.php';
				}
				$fedex 			= new Xa_Label_For_Vendor_Fedex_Support( $order, $xa_current_user );
				//
			}

			// Create UPS Label
			elseif( ! empty($_GET['xa_create_vendor_ups_label']) ) {
				list( $order_id, $user_id ) = explode( '|', base64_decode($_GET['xa_create_vendor_ups_label']) );
				//
			}
			
		}

		/**
		 * Print Labels for the particular Shipment Id.
		 */
		public function print_labels() {
			if( ! empty($_GET['xa_print_vendor_fedex_label']) ) {
				list( $order_id, $user_id, $shipment_id ) = explode( '|', base64_decode($_GET['xa_print_vendor_fedex_label']) );
				if( ! class_exists('Xa_Print_Vendor_Fedex_Label') ) {
					require_once 'fedex/class-xa-label-for-vendor-fedex-support.php';
				}
				new Xa_Print_Vendor_Fedex_Label( $order_id, $user_id, $shipment_id );
			}
		}

		/**
		 * Print Fedex Return Label.
		 */
		public function print_return_label(){
			if( ! class_exists('Xa_Print_Vendor_Fedex_Label') ) {
				require_once 'fedex/class-xa-label-for-vendor-fedex-support.php';
			}
			Xa_Print_Vendor_Fedex_Label::print_fedex_return_label();
		}

		public function my_custom_flush_rewrite_rules() {
			flush_rewrite_rules();
		}
		public function my_custom_endpoints() {
			add_rewrite_endpoint( 'xa-all-order', EP_ROOT | EP_PAGES );
		}

		public function my_custom_query_vars($vars) {
			$vars[] = 'special-page';

    		return $vars;
		}

		public function add_navigation_menu( $items ) {
			$items['xa-all-order'] = 'All-Order';
			return $items;
		}

		public function my_custom_endpoint_content() {
			global $xa_current_user;
			// Limit here based on the user capabilities
			$status = $this->check_permission($xa_current_user);
			if($status) {
				include 'xa-vendor-common.php';
			}
		}

		public function check_permission($xa_current_user) {
			$current_roles = $xa_current_user->roles;
			$allowed_roles = apply_filters( 'xa_generate_vendor_label_role_permission', array('administrator','wc_product_vendors_admin_vendor', 'vendor'), $xa_current_user );
			$compare_role = array_intersect( $current_roles, $allowed_roles);
			if( ! empty($compare_role) ) {
				return true;
			}
			else {
				return false;
			}
		}

		public function print_label( $order_id ) {
			$order = wc_get_order($order_id);
			if( ! class_exists('Xa_Label_For_Vendor_Fedex_Support') ) {
				require_once 'class-xa-label-for-vendor-fedex-support.php';
			}
			$fedex = new Xa_Label_For_Vendor_Fedex_Support($order);
			// $this->xa_fedex_plugin_support( $order );
		}

	}		//End of Class Xa_Label_For_Vendor_Print_Labels
	new Xa_Label_For_Vendor_Print_Labels();

}