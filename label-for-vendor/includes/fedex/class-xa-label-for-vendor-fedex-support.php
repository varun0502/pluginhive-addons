<?php

if( ! class_exists('Xa_Label_For_Vendor_Fedex_Support') ){
	
	class Xa_Label_For_Vendor_Fedex_Support {

		public function __construct() {

			global $xa_current_user;
			$this->user 		= $xa_current_user;
			$this->user_id 		= $this->user->ID;
			$this->order_id 	= isset($_GET['wf_fedex_createshipment']) ? $_GET['wf_fedex_createshipment'] : $_GET['wf_create_return_label'];
			$this->service_code = $_GET['create_shipment_service'];
			$this->order 		= wc_get_order($this->order_id);

			if( ! class_exists('wf_fedex_woocommerce_shipping_admin_helper') ) {
				require_once ABSPATH.'wp-content/plugins/fedex-woocommerce-shipping/includes/class-wf-fedex-woocommerce-shipping-admin-helper.php';
			}

			add_filter( 'wf_user_permission_roles', array( $this, 'xa_give_permission_to_vendor_to_create_label' ) );
			add_filter( 'xa_fedex_settings', array($this, 'override_fedex_settings_by_vendor_details') );
			// add_filter( 'wf_filter_label_packages', array( $this, 'xa_modify_the_package_based_on_product_authhor'), 10, 1 );
			if( ! empty($_GET['wf_create_return_label']) ) {
				$this->wf_create_return_shipment( $_GET['ph_fedex_shipment_id'], $this->order_id);
			}
			else{
				$this->create_shipment();
			}
		}

		public function create_shipment() {
			$fedex_admin 		= new wf_fedex_woocommerce_shipping_admin_helper();
			$this->fedex_admin 	= $fedex_admin;
			$fedex_admin->order = $this->order;
			$packages 			= $this->xa_modify_the_package_based_on_product_author( $fedex_admin->wf_get_package_from_order($this->order) );

			// Get Fedex Packages
			foreach( $packages as $package ) {
				$fedex_packages[] = $fedex_admin->get_fedex_packages($package);
			}
	
			foreach( $fedex_packages as $key => $fedex_package ) {

				// Assign service to every package
				foreach( $fedex_package as $fedex_package_key => $package ) {
					$fedex_packages[$key][$fedex_package_key]['service'] = $this->service_code;
					$fedex_package[$fedex_package_key]['service'] = $this->service_code;
				}

				// $fedex_admin->print_label_processor($fedex_package, current($packages) );
				$package = current($packages);
				$fedex_admin->residential_address_validation( $package );
				$request_type= '';
				if(! empty( $this->smartpost_hub ) && $package['destination']['country'] == 'US' && $this->service_code == 'SMART_POST'){
					$request_type = 'smartpost';
				}elseif(strpos($this->service_code, 'FREIGHT') !== false){
					$request_type = 'freight';
				}

				if($fedex_admin->validate_package($fedex_package)){
					$fedex_requests   = $fedex_admin->get_fedex_requests( $fedex_package, $package, $request_type);

					if( $package['destination']['country'] != $this->origin_country ) {
						$this->is_international = true;
					}
					if ( $fedex_requests ) {
						$this->run_package_request( $fedex_requests );
					}
					$packages_to_quote_count = sizeof( $fedex_requests );
				}


			}

		}

		/**
		 * Provide permission to generate Label in Fedex
		 * @param $roles array Array of User roles
		 * @return array Array of roles
		 */
		public function xa_give_permission_to_vendor_to_create_label( $roles ) {
			$roles[] = current($this->user->roles);
			return $roles;
		}

		/**
		 * Get the package of this vendor only.
		 */
		public function xa_modify_the_package_based_on_product_author( $packages ) {
			foreach( $packages as $vendor_id => $vendor_package ) {
				$count = 0;
				foreach( $vendor_package['contents'] as $key => $product_data ) {

					$product_author_id = $this->get_product_author_id($product_data['data']);
					if( $product_author_id != $this->user->ID ) {
						unset($vendor_package['contents'][$key]);
						$count++;
					}
				}
				if( $count == count($packages[$vendor_id]['contents']) ) {
					unset($packages[$vendor_id]);
				}
			}
			return $packages;
		}

		public function get_product_author_id( $product ) {
			$product_id = is_object($product) ? $product->get_id() : $product;
			$post 		= get_post($product_id);
			return $post->post_author;
		}

		public function run_package_request( $requests ) {
			$first_package = true;
			foreach ( $requests as $key => $request ) {
				if( $first_package ) {
					if( $this->commercial_invoice && $this->is_international ) {
						$company_logo = !empty($this->settings['company_logo']) ? true : false;
						$digital_signature = !empty($this->settings['digital_signature']) ? true : false;

						$special_servicetypes = !empty($request['RequestedShipment']['SpecialServicesRequested']['SpecialServiceTypes']) ? $request['RequestedShipment']['SpecialServicesRequested']['SpecialServiceTypes'] : array();
						array_unshift( $special_servicetypes, 'ELECTRONIC_TRADE_DOCUMENTS' );
						$request['RequestedShipment']['SpecialServicesRequested']['SpecialServiceTypes'] = $special_servicetypes;
						
						$request['RequestedShipment']['SpecialServicesRequested']['EtdDetail']['RequestedDocumentCopies'] = 'COMMERCIAL_INVOICE';
						$request['RequestedShipment']['ShippingDocumentSpecification']['ShippingDocumentTypes'] = 'COMMERCIAL_INVOICE';
						$request['RequestedShipment']['ShippingDocumentSpecification']['CommercialInvoiceDetail']['Format']['ImageType'] = 'PDF';
						$request['RequestedShipment']['ShippingDocumentSpecification']['CommercialInvoiceDetail']['Format']['StockType'] = 'PAPER_LETTER';
						
						if($company_logo){
							$request['RequestedShipment']['ShippingDocumentSpecification']['CommercialInvoiceDetail']['CustomerImageUsages'][] = array(
								'Type' 	=> 'LETTER_HEAD', 
								'Id' 	=> 'IMAGE_1', 
							);
						}

						if($digital_signature){
							$request['RequestedShipment']['ShippingDocumentSpecification']['CommercialInvoiceDetail']['CustomerImageUsages'][] = array(
								'Type' 	=> 'SIGNATURE',
								'Id' 	=> 'IMAGE_2',
							);
						}
					}
					$result = $this->fedex_admin->get_result( $request );
					$this->process_result( $result , $request );
				} else {
					$result = $this->fedex_admin->get_result( $request );
					$this->process_result( $result, $request );
				}
				$first_package =  false;
			}
			
			$url = get_permalink( get_option('woocommerce_myaccount_page_id') );
			wp_redirect( $url.'xa-all-order/?xa_view_order_on_front_end='.$this->order_id );
			exit;
		}

		private function process_result( $result = '' , $request) {
			if(!$result)
				return false;
			
			if ( $result->HighestSeverity != 'FAILURE' && $result->HighestSeverity != 'ERROR' && ! empty ($result->CompletedShipmentDetail) ) {
				
				if( property_exists($result->CompletedShipmentDetail,'CompletedPackageDetails') ){
					if(is_array($result->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds)){
						foreach($result->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds as $track_ids){
							if($track_ids->TrackingIdType != 'USPS'){
								$shipmentId = $track_ids->TrackingNumber;	
								$tracking_completedata = $track_ids; 		
							}else{
								$usps_shipmentId = $track_ids->TrackingNumber;
							}
						}
					}
					else{
						$shipmentId = $result->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds->TrackingNumber;		
						$tracking_completedata = $result->CompletedShipmentDetail->CompletedPackageDetails->TrackingIds;
					}	
				}
				elseif(property_exists($result->CompletedShipmentDetail,'MasterTrackingId')){
					$shipmentId = $result->CompletedShipmentDetail->MasterTrackingId->TrackingNumber;		
					$tracking_completedata = $result->CompletedShipmentDetail->MasterTrackingId;				
				}			
				
				//if return label
				if( !empty($this->shipmentId) && property_exists($result->CompletedShipmentDetail->CompletedPackageDetails->Label,'ShippingDocumentDisposition') && $result->CompletedShipmentDetail->CompletedPackageDetails->Label->ShippingDocumentDisposition == 'RETURNED'){
					
					$package_shipping_label = $result->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image;
					if(base64_encode(base64_decode($package_shipping_label, true)) === $package_shipping_label){  //For nusoap encoded label response
						$return_label = $package_shipping_label;
					}
					else{
						$return_label = base64_encode($package_shipping_label);
					}
					$returnlabel_type = $result->CompletedShipmentDetail->CompletedPackageDetails->Label->ImageType; //Shipment ImageType

					add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_returnShipmetId'.$this->shipmentId, $shipmentId, true);
					add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_returnLabel_'.$shipmentId, $return_label, true);
					if( !empty($returnlabel_type) ){
						 add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_returnLabel_image_type_'.$shipmentId, $returnlabel_type, true);
					}
					return;				
				}
				
				if( !empty($result->CompletedShipmentDetail->MasterTrackingId) && empty($this->master_tracking_id) )
					$this->master_tracking_id = $result->CompletedShipmentDetail->MasterTrackingId;
					$this->fedex_admin->master_tracking_id = $result->CompletedShipmentDetail->MasterTrackingId;
				
				$addittional_label = array();
				$addittional_label_type = array();
				if(property_exists($result->CompletedShipmentDetail,'CompletedPackageDetails')){
					$package_shipping_label=$result->CompletedShipmentDetail->CompletedPackageDetails->Label->Parts->Image;
					if(base64_encode(base64_decode($package_shipping_label, true)) === $package_shipping_label){  //For nusoap encoded label response
						$shippingLabel = $package_shipping_label;
					}
					else{
						$shippingLabel = base64_encode($package_shipping_label);
					}
					$shippinglabel_type = $result->CompletedShipmentDetail->CompletedPackageDetails->Label->ImageType; //Shipment ImageType
					
					if(property_exists($result->CompletedShipmentDetail->CompletedPackageDetails,'PackageDocuments')){
						$package_documents = $result->CompletedShipmentDetail->CompletedPackageDetails->PackageDocuments;
						if(is_array($package_documents)){
							foreach($package_documents as $document_key=>$package_document){
								$package_additional_label = $package_document->Parts->Image;
								if(base64_encode(base64_decode($package_additional_label, true)) === $package_additional_label){
									$addittional_label[$document_key] = $package_additional_label;
								}else{
									$addittional_label[$document_key] = base64_encode($package_additional_label);
								}
								$addittional_label_type[$document_key] = $package_document->ImageType;
							}
						}
					}
					
					
					if(property_exists($result->CompletedShipmentDetail,'ShipmentDocuments')){
						$commercial_invoice_label=$result->CompletedShipmentDetail->ShipmentDocuments->Parts->Image;
						if(base64_encode(base64_decode($commercial_invoice_label, true)) === $commercial_invoice_label){
							$addittional_label['Commercial Invoice'] = $commercial_invoice_label;
						}else{
							$addittional_label['Commercial Invoice'] = base64_encode($commercial_invoice_label);
						}
						$addittional_label_type['Commercial Invoice'] = $result->CompletedShipmentDetail->ShipmentDocuments->ImageType;
					}
				} 
				elseif(property_exists($result->CompletedShipmentDetail,'ShipmentDocuments')){ 
					//As per the documentation. This case will never occure. 
					$shipment_document_label = $result->CompletedShipmentDetail->ShipmentDocuments->Parts->Image;
					if(base64_encode(base64_decode($shipment_document_label, true)) === $shipment_document_label){
						$shippingLabel = $shipment_document_label;
					}
					else{
						$shippingLabel = base64_encode($shipment_document_label);
					}
					$shippinglabel_type = $result->CompletedShipmentDetail->ShipmentDocuments->ImageType;
				}
				
				if(!empty($shippingLabel) && property_exists($result->CompletedShipmentDetail,'AssociatedShipments')){
					$associated_documents = $result->CompletedShipmentDetail->AssociatedShipments->Label;
					if(!empty($associated_documents)){
						
							$associated_shipment_label = $associated_documents->Parts->Image;
							if(base64_encode(base64_decode($associated_shipment_label, true)) === $associated_shipment_label){
								$addittional_label['AssociatedLabel'] = $associated_shipment_label;
							}
							else{
								$addittional_label['AssociatedLabel'] = base64_encode($associated_shipment_label);
							}
							$addittional_label_type['AssociatedLabel'] = $associated_documents->ImageType;
					}
				}
				
				 if(!empty($shipmentId) && !empty($shippingLabel)){
					add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_shipmentId', $shipmentId, false);
					add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_shippingLabel_'.$shipmentId, $shippingLabel, true);
					add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_packageDetails_'.$shipmentId, $this->fedex_admin->wf_get_parcel_details($request) , true);
					add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_request_'.$shipmentId, $request , true);

					if( !empty($shippinglabel_type) ){
						 add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_shippingLabel_image_type_'.$shipmentId, $shippinglabel_type, true);
					}
					
					if(isset($tracking_completedata)){
						add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_tracking_full_details_'.$shipmentId, $tracking_completedata, true);
					}			
						
					if( !empty($request['RequestedShipment']['ServiceType']) ){
						add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_service_code'.$shipmentId, $request['RequestedShipment']['ServiceType'], true);
					}
					
					if(!empty($usps_shipmentId)){
						add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_usps_trackingid_'.$shipmentId, $usps_shipmentId, true);
					}

					if($this->add_trackingpin_shipmentid == 'yes' && !empty($shipmentId)){
						//$this->order->add_order_note( sprintf( __( 'Fedex Tracking-pin #: %s.', 'wf-shipping-fedex' ), $shipmentId) , true);
						$this->tracking_ids = $this->tracking_ids . $shipmentId . ',';			
					}
					
					if($this->add_trackingpin_shipmentid == 'yes' && !empty($usps_shipmentId)){
						//$this->order->add_order_note( sprintf( __( 'Fedex Smart Post USPS Tracking-pin #: %s.', 'wf-shipping-fedex' ), $usps_shipmentId) , true);
					}
					
					if(!empty($addittional_label)){
						add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_fedex_additional_label_'.$shipmentId, $addittional_label, true);	
						if(!empty($addittional_label_type)){
							add_post_meta($this->order_id, 'wf_vendor_'.$this->user_id.'_fedex_additional_label_image_type_'.$shipmentId, $addittional_label_type, true);		
						}	
					}							
				} 
				do_action('xa_fedex_label_generated_successfully',$shipmentId,$shippingLabel,$this->order_id);
			}else{
				$this->shipmentErrorMessage .=  $this->fedex_admin->result_notifications($result->Notifications, $error_message='');
			}
		}

		/**
		 * Override Fedex settings by vendor details .
		 */
		public function override_fedex_settings_by_vendor_details( $settings) {

			// To get the vendor address
			$this->get_vendor_details();
			$this->settings 							= $settings;

			// Override Fedex Api Credentials
			$this->settings['account_number'] 			= get_the_author_meta( 'xa_fedex_account_number', $this->user->ID );
			$this->settings['meter_number'] 			= get_the_author_meta( 'xa_fedex_meter_number', $this->user->ID );
			$this->settings['api_key'] 					= get_the_author_meta( 'xa_fedex_web_services_key', $this->user->ID );
			$this->settings['api_pass'] 				= get_the_author_meta( 'xa_fedex_web_services_password', $this->user->ID );

			// Override address with vendor address
			$this->settings['shipper_person_name'] 		= $this->vendor_address['first_name'].' '. $this->vendor_address['last_name'];
			$this->settings['shipper_company_name']		= $this->vendor_address['company'];
			$this->settings['shipper_phone_number']		= $this->vendor_address['phone'];
			$this->settings['frt_shipper_street']		= $this->vendor_address['address_1'];
			$this->settings['shipper_street_2']			= $this->vendor_address['address_2'];
			$this->settings['origin']					= $this->vendor_address['postcode'];
			$this->settings['freight_shipper_city']		= $this->vendor_address['city'];
			$this->settings['origin_country']			= $this->vendor_address['country'].':'.$this->vendor_address['state'];
			$this->settings['shipper_email']			= $this->vendor_address['email'];

			// Override Tax Payor Identification number
			$this->settings['tin_number']				= $this->vendor_address['tin_number'];

			$this->commercial_invoice 			= (isset($this->settings['commercial_invoice']) && ($this->settings['commercial_invoice'] == 'yes')) ? true : false;
			$this->origin_country 				= $this->vendor_address['country'];
			return $this->settings;
		}

		/**
		 * Get Vendor details, like address.
		 */
		public function get_vendor_details() {
			require_once plugin_dir_path(__DIR__).'class-xa-common-vendor-details.php';
			$vendor_common_details 			= new Xa_Common_vendor_details();
			$vendor_address 				= $vendor_common_details->get_vendor_address($this->user->ID);
			$this->vendor_address 			= $vendor_common_details ->wf_formate_origin_address($vendor_address);
		}

		public function wf_fedex_viewlabel(){
			$shipmentDetails = explode('|', base64_decode($_GET['xa_view_fedex_label_vendor']));

			if (count($shipmentDetails) != 2) {
				exit;
			}
			
			$shipmentId = $shipmentDetails[0]; 
			$post_id = $shipmentDetails[1]; 
			$shipping_label = get_post_meta($post_id, 'wf_woo_fedex_shippingLabel_'.$shipmentId, true);
			$shipping_label_image_type = get_post_meta($post_id, 'wf_woo_fedex_shippingLabel_image_type_'.$shipmentId, true);
			
			
			if( empty($shipping_label_image_type) ){
				$shipping_label_image_type = $this->image_type;
			}
			$file_name = wp_upload_dir();
			$filename  = $file_name['path']."/fedex_shipment_label_$shipmentId.$shipping_label_image_type";
			file_put_contents( $filename, base64_decode($shipping_label) );
		}

		public function wf_create_return_shipment( $shipment_id, $order_id ){
			$this->shipmentId = $shipment_id;
			$request = get_post_meta( $order_id, 'wf_vendor_'.$this->user_id.'_woo_fedex_request_'.$shipment_id, true );
			if( ! empty($request) ){
				$request['RequestedShipment']['ServiceType'] 				= $this->service_code;

				$shipper_address = $request['RequestedShipment']['Shipper'];
				$request['RequestedShipment']['Shipper'] 					= $request['RequestedShipment']['Recipient'];
				$request['RequestedShipment']['Recipient']					= $shipper_address;

				$total_weight = 0;
				foreach ($request['RequestedShipment']['RequestedPackageLineItems'] as $key => $item) {
					$request['RequestedShipment']['RequestedPackageLineItems'][$key]['SequenceNumber'] = 1;
					$request['RequestedShipment']['RequestedPackageLineItems'][$key]['GroupNumber'] = 1;
					$total_weight += $item['Weight']['Value'];
				}
				$request['RequestedShipment']['TotalWeight']['Value']		= $total_weight;


				$request['RequestedShipment']['SpecialServicesRequested']['ReturnShipmentDetail']['ReturnType']	= 'PRINT_RETURN_LABEL';
				// $request['RequestedShipment']['SpecialServicesRequested']['ReturnShipmentDetail']['ReturnEMailDetail']['MerchantPhoneNumber']	= '';
				$request['RequestedShipment']['SpecialServicesRequested']['SpecialServiceTypes'][] = 'RETURN_SHIPMENT';
				
				$request['RequestedShipment']['PackageCount'] 				= 1;
				unset($request['RequestedShipment']['RequestedPackageLineItems']['SequenceNumber'], $request['RequestedShipment']['SpecialServicesRequested']['CodDetail'] );
				foreach( $request['RequestedShipment']['SpecialServicesRequested']['SpecialServiceTypes'] as $key => $special_service ) {
					if( $special_service == 'COD' ) {
						unset($request['RequestedShipment']['SpecialServicesRequested']['SpecialServiceTypes'][$key]);			// Unset COD in return request
					}
				}

			}
			$fedex_admin 		= new wf_fedex_woocommerce_shipping_admin_helper();
			$this->fedex_admin 	= $fedex_admin;
			$fedex_admin->order = $this->order;
			$result = $this->fedex_admin->get_result( $request );
			$this->process_result($result, $request );
			$url = get_permalink( get_option('woocommerce_myaccount_page_id') );
			wp_redirect( $url.'xa-all-order/?xa_view_order_on_front_end='.$this->order_id );
			exit;
		}
	}
}

if( ! class_exists('Xa_Print_Vendor_Fedex_Label') ) {
	class Xa_Print_Vendor_Fedex_Label {


		public function __construct( $order_id, $user_id, $shipment_id ) {
			$this->print_label( $order_id, $user_id, $shipment_id );
		}

		/**
		 * Print Fedex Label
		 */
		public function print_label( $order_id, $user_id, $shipment_id ) {

			$shipping_label_image_type 	= get_post_meta( $order_id, 'wf_vendor_'.$user_id.'_woo_fedex_shippingLabel_image_type_'.$shipment_id, true );
			$shipping_label 			= get_post_meta( $order_id, 'wf_vendor_'.$user_id.'_woo_fedex_shippingLabel_'.$shipment_id, true );

			header('Content-Type: application/'.$shipping_label_image_type);
			header('Content-disposition: attachment; filename="ShipmentArtifact-' . $shipment_id . '.'.$shipping_label_image_type.'"');
			print(base64_decode($shipping_label)); 
			exit;
		}

		public static function print_fedex_return_label(){
			list( $order_id, $user_id, $shipment_id ) = explode( '|', base64_decode($_GET['xa_print_vendor_fedex_return_label']) );
			$shipping_label_image_type 	= get_post_meta( $order_id, 'wf_vendor_'.$user_id.'_woo_fedex_returnLabel_image_type_'.$shipment_id, true );
			$shipping_label 			= get_post_meta( $order_id, 'wf_vendor_'.$user_id.'_woo_fedex_returnLabel_'.$shipment_id, true );

			header('Content-Type: application/'.$shipping_label_image_type);
			header('Content-disposition: attachment; filename="ShipmentArtifact-' . $shipment_id . '.'.$shipping_label_image_type.'"');
			print(base64_decode($shipping_label)); 
			exit;
		}
	}	// End of Class
}