<?php

$order_posts = get_posts( array(
    'numberposts' => -1,
    'post_type'   => wc_get_order_types(),
    'post_status' => array_keys( wc_get_order_statuses() ),
) );

$wp_date_format = get_option('date_format');
$url = get_permalink( get_option('woocommerce_myaccount_page_id') );

echo "<table>";
	echo "<tr>";
		echo "<th>Order</th>";
		echo "<th>Date</th>";
		echo "<th>Status</th>";
		echo "<th>Actions</th>";
	echo "</tr>";
		foreach( $order_posts as $order_post ) {
			$order 		= wc_get_order( $order_post );
			$view_url 	= $url."xa-all-order/?xa_view_order_on_front_end=".$order_post->ID;
			echo "<tr>";
				echo "<td>".$order->get_order_number()."</td>";
				$date = date_create($order_post->post_date);
				$date = date_format( $date, $wp_date_format);
				echo "<td>".$date."</td>";
				echo "<td>".$order_post->post_status."</td>";
				?>

				<td><a style="margin: 4px 4px;" class="button button-primary tips onclickdisable xa_view_order_on_front_end" href="<?php echo $view_url; ?>">	<?php _e('View', 'wf-shipping-fedex'); ?></a><hr style="border-color:#0074a2"></td>

				<?php
			echo "</tr>";
		}
echo "</table>";