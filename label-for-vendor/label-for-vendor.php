<?php

/*
	Plugin Name: Woocommerce Label Print option for Vendors
	Plugin URI: 
	Description: Give the power to print the label to your vendors for the products in the order
	Version: 1.0.1
	Author: varun874
	Author URI: https://www.xadapter.com/vendor/wooforce/
	WC requires at least: 3.4
	WC tested up to: 3.4
*/


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Setting active plugins as global variable for further usage
global $xa_active_plugins;
$xa_active_plugins = get_option( 'active_plugins');


register_activation_hook( __FILE__, function() {

	global $xa_active_plugins;
	// Activate only if woocommerce is active
	if ( empty($xa_active_plugins) || ! in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', $xa_active_plugins ) )) {
		deactivate_plugins( basename( __FILE__ ) );
		wp_die( __("Oops! You installing Woocommerce Label Print option for Vendors without active woocommerce plugin.", "x" ), "", array('back_link' => 1 ));
	}
});

if ( ! defined('WF_Fedex_ID') ) {
	define("WF_Fedex_ID", "wf_fedex_woocommerce_shipping");
}

if(!defined('VENDOR_PLUGIN') ){
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	$plugin_list = array(
		'product_vendor' => 'woocommerce-product-vendors/woocommerce-product-vendors.php', 
		'dokan_lite' => 'dokan-lite/dokan.php', 
		'wf_product_vendor' => 'wf-product-vendor/product-vendor-map.php',
		'wc_vendors_pro' => 'wc-vendors-pro/wcvendors-pro.php'
	);
	foreach ($plugin_list as $plugin_name => $slug) {
		if ( is_plugin_active($slug) ){
			define('VENDOR_PLUGIN',$plugin_name);
			break;
		}
	}
}

// If woocommerce is active
if (in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', $xa_active_plugins ) )) {

	// Get Settings url
	if ( ! function_exists('xa_get_settings_url') ) {
		function xa_get_settings_url() {
			return version_compare(WC()->version, '2.1', '>=') ? "wc-settings" : "woocommerce_settings";
		}
	}

	if (!function_exists('xa_plugin_override')){
		add_action( 'plugins_loaded', 'xa_plugin_override' );
		function xa_plugin_override() {
			if (!function_exists('WC')){
				function WC(){
					return $GLOBALS['woocommerce'];
				}
			}
		}
	}

	if( ! class_exists('Xa_Print_Label_Option_To_Vendor') ) {
		class Xa_Print_Label_Option_To_Vendor {

			public function __construct() {
				
				require_once 'includes/class-xa-label-for-vendor-print-labels.php';
			}



		}	// End of Class Xa_Print_Label_Option_To_Vendor

		new Xa_Print_Label_Option_To_Vendor();
	}

}