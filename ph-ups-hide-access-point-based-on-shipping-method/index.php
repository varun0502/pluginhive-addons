<?php
/**
 * Plugin Name: UPS Access Point on Checkout
 * Plugin URI: https://pluginhive.com/
 * Description: It is an addon for UPS plugin provided by PluginHive. It hides UPS AccessPoint on checkout. If any other services is getting selected apart from UPS.
 * Version: 1.0.0
 * Author: Varun
 * Author URI: https://pluginhive.com
 */


// Exit if accessed Directly
if( ! defined('ABSPATH') ) {
	exit;
}

/**
 * Hide Accesspoint dropdown on checkout page.
 */
add_action( 'woocommerce_review_order_before_shipping', function(){
	?>
		<script>
			let shipping_method = jQuery('input[class=shipping_method]:checked').val();
			if( typeof shipping_method != 'undefined' ) {
				if( shipping_method.search('wf_shipping_ups') == -1 )
					jQuery("#shipping_accesspoint_field").hide();
				else
					jQuery("#shipping_accesspoint_field").show();
			}
		</script>
	<?php
});