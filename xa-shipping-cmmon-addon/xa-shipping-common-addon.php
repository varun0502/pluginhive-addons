<?php
/*
Plugin Name: Customize Shipping Options
Description: Addon to customize/change various shipping options (like shipping address) of xadapter plugins.
Version: 1.3.5
Author: pluginhive
Author URI: https://www.pluginhive.com/
Supported plugin(s): FedEx, Canadapost, Stamps
*/

if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly.
}	

//check if woocommerce exists
if ( !class_exists( 'woocommerce' ) ) {   
	add_action( 'admin_init', 'wf_shipping_addon_plugin_deactivate' );
	if ( ! function_exists( 'wf_shipping_addon_plugin_deactivate' ) ) {
		function wf_shipping_addon_plugin_deactivate() {
			  	if ( !class_exists( 'woocommerce' ) ){
				   deactivate_plugins( plugin_basename( __FILE__ ) );
				   wp_safe_redirect( admin_url('plugins.php') );
								   
			  	}
		}
	}
}


//Class - To setup the plugin
class Wf_shipping_addon_Setup {
		//constructor
	public function __construct() {
		$this->wf_shipping_addon_init();
		$this->xa_shipping_filters();
		add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'wf_shipping_addon_plugin_action_links' ) );
		//add_action('woocommerce_get_settings_pages',array($this, 'wf_shipping_addon_initialize'));
		

		add_filter('woocommerce_shipping_fields',array( $this, 'wf_add_checkout_page_field' ) );
		add_filter('woocommerce_billing_fields',array( $this, 'wf_add_checkout_page_field' ) );
        add_filter( 'woocommerce_cart_shipping_packages', array( $this, 'woocommerce_cart_shipping_packages' ));
	}

	function woocommerce_cart_shipping_packages($shipping){
	    if(isset($_POST['post_data'])){
	        parse_str($_POST['post_data'],$str);
	        if(isset($str['eha_is_residential'])){
	            
	            foreach($shipping as $key=>$val){
	                $shipping[$key]['destination']['is_residential']=true;
	            }
	        }
	        else{
	            foreach($shipping as $key=>$val){
	                $shipping[$key]['destination']['is_residential']=false;
	            }
	        }
	    }
	    return $shipping;
	}

	public function wf_add_checkout_page_field($fields){
    	$saved_matrix = get_option('wf_shipping_addon_rate_role_matrix');
    	if( !$saved_matrix[0]['enable_checkout_residencial'] ){
    		return $fields;
    	}

		$out_f=array();
	    $done=false;
	    foreach($fields as $key=>$val){
	        if($key=='billing_address_2' || $key=='shipping_address_2'){
	            $out_f[$key]=$val;
	            $out_f['eha_is_residential']=array(
	               'type' => 'checkbox',
	               'label' => __('This is a Residential Address', 'woocommerce'),
	               'class'         => array('my-field-class form-row-wide','update_totals_on_change'),
	               'priority'=>1
	            );                  
	            $done=true;
	        }else{
	            $out_f[$key]=$val;
	        }
	       
	    }
	    if(!$done){
	         $out_f['eha_is_residential']=array(
               'type' => 'checkbox',
               'label' => __('This is a Residential Address', 'woocommerce'),
               'class'         => array('my-field-class form-row-wide','update_totals_on_change'),
               'priority'=>1
            );                  
            $done=true;
	    }
	 	return $out_f;   
	}
	

	private function xa_shipping_filters(){
		
		//Filter for switching origin address based on destination country.
		// Supported plugin(s): FedEx
		add_filter('wf_filter_label_from_address', array( $this, 'xa_new_origin_addres'), 8, 2 );

		//Filter for sending custom text to print on shipping label of stamps plugin
		add_filter('wf_stamps_request', array($this,'add_custom_text'),10,2);		
		
	}

	public function add_custom_text($request, $order){
		//Function to print the custom text on shipping label of Stamps	
		$origin_address_datas = get_option('wf_shipping_addon_label_options');
		if( !empty($origin_address_datas[0]['custom_text']) ){
			$text=str_replace('[orderid]', $order->id, $origin_address_datas[0]['custom_text'] );
			$request['printMemo']='1';					//Set it to true to print the text of memo field
			$request['memo'] = $text;	//Send Custom text to print the custom text on shipping label in stamp
			return $request;
		}
		return $request;
	}

	public function xa_new_origin_addres($from_address , $package){
		$new_settings = get_option( 'wf_shipping_addon_label_options', null );
		if( empty($new_settings) ){
			$new_settings = array();
		}
		$found = false;
		foreach ($new_settings as $key => $value) {
			if( $value['des_country'] == $package['destination']['country'] ){
				if( empty($value['des_states']) ) {
					$found = $value;
					break;
				}
				elseif( in_array($package['destination']['state'], $value['des_states']) ){
					$found = $value;
					break;
				}
			}
		}
		if($found){
			return array(
				'name'		=> $found['origin_address_name'],
				'company' 	=> $found['origin_address_company'],
				'phone' 	=> $found['origin_address_phone'],
				'email' 	=> $found['origin_address_email'],
				'address_1' => $found['origin_address_address_1'],
				'address_2' => $found['origin_address_address_2'],
				'city' 		=> $found['origin_address_city'],
				'state' 	=> $found['origin_address_state'],
				'country' 	=> $found['origin_address_country'],
				'postcode' 	=> $found['origin_address_postcode'],
			);
		}
		return $from_address;
	}

	public function wf_get_settings_url(){
		return version_compare(WC()->version, '1.0', '>=') ? "wc-settings" : "woocommerce_settings";
	}
		
	//to add settings url near plugin under installed plugin
	public function wf_shipping_addon_plugin_action_links( $links ) {
		$plugin_links = array(
			'<a href="' . admin_url( 'admin.php?page=' . $this->wf_get_settings_url() . '&tab=wf_shipping_addon' ) . '">' . __( 'Settings', 'wf_shipping_addon' ) . '</a>',

		);
		return array_merge( $plugin_links, $links );
	} 
	//to include the necessary files for plugin
	public function wf_shipping_addon_init() {
		include_once( 'includes/class-xa-shipping-addon-settings.php' );
		include_once( 'includes/class-xa-shipping-rates-processor.php' );
	}		

}
new Wf_shipping_addon_Setup();

