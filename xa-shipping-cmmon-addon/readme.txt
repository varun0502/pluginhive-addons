=== XA Shipping Common addon Plugin ===
Contributors: pluginhive
Stable tag: 1.3.5
Tags: WooCommerce, Wordpress, Shipping
Requires at least: 3.0.1
Tested up to: 4.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==

= Introduction =

<blockquote>

= Important Note: = This is NOT an independent plugin. To use this plugin, you need to have :
1.  <a rel="nofollow" href="https://www.xadapter.com/product/woocommerce-fedex-shipping-plugin-with-print-label/" target="_blank">WooForce FedEx Shipping Plugin</a>,  
<a rel="nofollow" href="https://www.xadapter.com/product/woocommerce-stamps-com-shipping-plugin-with-usps-postage/" target="_blank">Stamps.com Shipping Plugin</a> or 
<a rel="nofollow" href="https://www.xadapter.com/product/woocommerce-dhl-shipping-plugin-with-print-label/" target="_blank">DHL Shipping Plugin.</a>

</blockquote>


= How does it work? =

Step 1: Install our FedEx or DHL shipping plugin. 
Step 2: Install this Add-on and configure it! 
You are done!

= About XAdapter.com =

[XAdapter.com](https://www.xadapter.com/) creates quality WordPress/WooCommerce plugins that are easy to use and customize. We are proud to have thousands of customers actively using our plugins across the globe.


== Installation ==

1. Upload the plugin folder to the /wp-content/plugins/ directory.
2. Activate the plugin through the Plugins menu in WordPress.
3. Thats it! you can now configure the plugin.

== Frequently Asked Questions ==

== Changelog ==

=1.3.3=
*initial version
= 1.3.5 =
* Added state to switch origin address.

== Upgrade Notice ==
=1.3.3=
*initial version
