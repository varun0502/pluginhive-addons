<?php
    $saved_matrix = get_option('wf_shipping_addon_rate_role_matrix');
?>
<table style="width:70%">
    <tr>
        <td style="width:30%"><strong>Enable residencial checkbox in checkout page</strong></td><td >
            <?php
            woocommerce_form_field(
                "wf_shipping_addon_rate_role_matrix[0][enable_checkout_residencial]", array(
                    'id'        => "enable_checkout_residencial",
                    'type'      => "checkbox",
                    'label'     => __("Enable"),
                ),$saved_matrix[0]['enable_checkout_residencial']
            ); ?>   
        </td>
    </tr>
</table>

<hr/>

<table class="wp-list-table widefat fixed posts role_matrix" style="width:68%">
    <thead>
        <tr>
            <th><?php  _e('Shipping class', 'wf_shipping_addon' ); ?></th>
            <th><?php  _e('Country', 'wf_shipping_addon'); ?></th>
            <th><?php  _e('State Code', 'wf_shipping_addon'); ?></th>
            <th><?php  _e('Shipping method', 'wf_shipping_addon'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $test = new WC_Shipping;
        $shipping_classes = !empty( $temp = $test->get_shipping_classes() ) ? $temp : array(); 
        

        $this->role_matrix = array();
        $i=0;
        if( empty($saved_matrix) ){
            foreach ( $shipping_classes as $id => $value ) {
                $this->role_matrix[$i]['shipping_class']    = '';
                $this->role_matrix[$i]['country']           = '';
                $this->role_matrix[$i]['state']             = '';
                $this->role_matrix[$i]['shipping_method']   = '';
                $i++;
            }
        } else {
            foreach ( $saved_matrix as $id => $matrix ) {
                // if( is_array($shipping_classes) && key_exists($id,$shipping_classes) ){
                if( is_array($shipping_classes) && !empty($matrix['shipping_method']) && $matrix['shipping_class'] ){
                    $this->role_matrix[$id]['shipping_class']   = $matrix['shipping_class'];
                    $this->role_matrix[$id]['country']          = $matrix['country'];
                    $this->role_matrix[$id]['state']            = $matrix['state'];
                    $this->role_matrix[$id]['shipping_method']  = $matrix['shipping_method'];
                }
            }
        }
		foreach ( $this->role_matrix as $key => $value ){
            global $woocommerce;
            if(!array_filter($value))
                continue;
            ?>
			<tr>
				<td>    
                    <?php
                    if( !empty($shipping_classes) ){?>
                        <select name="wf_shipping_addon_rate_role_matrix[<?php  echo $key; ?>][shipping_class]"><?php
                        echo"<option value=''>-Select One-</option>";
                        foreach ($shipping_classes as $id => $shipping_class) {
                            if( $this->role_matrix[ $key ]['shipping_class'] == $shipping_class->name ){
                                echo"<option value='$shipping_class->name' selected>$shipping_class->name</option>";
                            }else{
                                echo"<option value='$shipping_class->name'>$shipping_class->name</option>";
                            }
                        }
                        echo "</select>";
                    }?>
				</td>
                <td>
                    <input type="text" placeholder="Eg: US" name="wf_shipping_addon_rate_role_matrix[<?php  echo $key; ?>][country]" value="<?php  echo isset( $this->role_matrix[ $key ]['country'] ) ? $this->role_matrix[ $key ]['country'] : ''; ?>"/>
                </td>
                <td >
                    <input type="text" placeholder="Eg: AL" name="wf_shipping_addon_rate_role_matrix[<?php  echo $key; ?>][state]" value="<?php  echo isset( $this->role_matrix[ $key ]['state'] ) ? $this->role_matrix[ $key ]['state'] : ''; ?>"/>
                </td>
                <td >
					<input type="text" placeholder="Eg: free_shipping:1" size="30" name="wf_shipping_addon_rate_role_matrix[<?php  echo $key; ?>][shipping_method]" value="<?php  echo isset( $this->role_matrix[ $key ]['shipping_method'] ) ? $this->role_matrix[ $key ]['shipping_method'] : ''; ?>"/>
				</td>
			</tr><?php 
		}?>
        <tr>
            <td>    
                <?php
                $key = empty($$key) ? 0 : $key+1;
                if( !empty($shipping_classes) ){?>
                    <select name="wf_shipping_addon_rate_role_matrix[<?php  echo $key; ?>][shipping_class]"><?php
                    echo"<option>--Select One--</option>";
                    foreach ($shipping_classes as $id => $shipping_class) {
                        echo"<option>$shipping_class->name</option>";
                    }
                    echo "</select>";
                }?>
            </td>
            <td>
                <input type="text" placeholder="Eg: US" name="wf_shipping_addon_rate_role_matrix[<?php  echo $key; ?>][country]" />
            </td>
            <td >
                <input type="text" placeholder="Eg: AL" name="wf_shipping_addon_rate_role_matrix[<?php  echo $key; ?>][state]" />
            </td>
            <td >
                <input type="text" size="30" name="wf_shipping_addon_rate_role_matrix[<?php  echo $key; ?>][shipping_method]" />
            </td>
        </tr>
	</tbody>
	 <div>
		<p>Hide shipping methods.
		</p>
	<div>
</table>