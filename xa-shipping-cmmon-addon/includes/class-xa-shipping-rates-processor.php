<?php
class XA_shipping_rates_processor{
    public function __construct() {
		add_filter('woocommerce_package_rates', array($this,'process_shipping_role_matrix'), 10, 2);
    }


    public function process_shipping_role_matrix($available_shipping_methods, $package){
    	$this->package = $package;
		$saved_matrix = !empty( $temp = get_option('wf_shipping_addon_rate_role_matrix') ) ? $temp : array();
		foreach ($saved_matrix as $key => $role) {
			if( isset($role['shipping_class'])
                && $this->is_shipping_class_exist( array($role['shipping_class']) )
				&& $this->is_country_exist($role['country'])
				&& $this->is_state_exist($role['state'])
			){	
				unset( $available_shipping_methods[ $role['shipping_method'] ] );
			}
		}
		return $available_shipping_methods;
    }

    private function is_state_exist($state){
    	if(empty($state))
    		return false;
    	return $state == $this->package['destination']['state'];
    }

    private function is_country_exist($country){
    	global $woocommerce;
    	if(empty($country))
    		return false;
    	return $country == $woocommerce->customer->get_shipping_country();
    }

    private function is_shipping_class_exist( $shipping_classes ){
    	if(empty($shipping_classes))
    		return false;

    	global $woocommerce;
    	foreach(WC()->cart->cart_contents as $key => $values) {
    	    if ( in_array($values['data']->get_shipping_class() , $shipping_classes) ) {
    	        return true;
    	        break;
    	    }
    	}
    	return false;
    }
}
new XA_shipping_rates_processor;
