<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if(!class_exists('WC_Settings_Page')){
	include_once( dirname(__FILE__)."/../../woocommerce/includes/admin/settings/class-wc-settings-page.php" );
}

class Wf_shipping_addon_Settings extends WC_Settings_Page {

    public function __construct() {    
	    $this->id    = 'wf_shipping_addon';
	    $this->label = __( 'Shipping Common addon', 'xa-shipping-addon' );
	    add_filter( 'woocommerce_settings_tabs_array',        array( $this, 'add_settings_page' ), 21 );
	    add_action( 'woocommerce_sections_' . $this->id,      array( $this, 'output_sections' ) );

	    add_action( 'woocommerce_settings_' . $this->id,      array( $this, 'wf_shipping_addon_output' ) );
	    add_action( 'woocommerce_settings_save_' . $this->id, array( $this, 'wf_shipping_addon_save' ) );

	    add_action( 'woocommerce_update_options_wf_shipping_addon', array( $this, 'wf_shipping_addon_update_settings') );

	    add_action('woocommerce_admin_field_origin_address',array( $this, 'generate_label_options_html')); 
	    add_action('woocommerce_admin_field_rate_options',array( $this, 'generate_rate_options_html'));

        add_action( 'current_screen', array( $this,'wf_shipping_addon_this_screen' ));
        add_action( 'wp_footer', array( $this, 'wf_shipping_addon_scripts' ) );	    	
	}
	
	public function get_sections() {  
	    $sections = array(
	        'origin_address'    => __( 'Label Options', 'xa-shipping-addon' ),
	        'xa_rate_options'         => __( 'Rate Options', 'xa-shipping-addon' ),
	    );	           
	    return apply_filters( 'woocommerce_get_sections_' . $this->id, $sections );
	}

	public function wf_shipping_addon_update_settings( $current_section ) {
		global $current_section;
		switch($current_section) {
			case 'origin_address':
				$options = $this->wf_shipping_addon_get_settings();
				woocommerce_update_options( $options );
				$origin_address_datas = get_option('wf_shipping_addon_label_options');
				break;
			case 'xa_rate_options':
				$options = $this->wf_shipping_addon_get_settings();
				woocommerce_update_options( $options );
				$rate_options_dates = get_option('wf_shipping_addon_rate_role_matrix');
				break;
		}
    }

	public function wf_shipping_addon_get_settings( $current_section = '' ) {
		global $current_section;
		switch($current_section){
			case '':
			case 'origin_address':
				$settings = apply_filters( 'wf_shipping_addon_section2_settings', array(
					'origin_address_options_title'	=>	array(
						'name' => __( 'Label Options', 'xa-shipping-addon' ),
						'type' => 'title',
						'desc' => '',
						'id'   => 'wf_shipping_addon_label_options_options_title',
					),	
					'origin_address'	=>	array(
					'type'     => 'origin_address',
					'id'       => 'wf_shipping_addon_label_options',
					),			
					'origin_address_options_sectionend'	=>	array(
						'type' => 'sectionend',
						'id'   => 'wf_shipping_addon_label_options_options_sectionend'
					),			
				) );			
				break;	
					
			case 'xa_rate_options':
				$settings = apply_filters( 'wf_shipping_addon_section3_settings', array(
					'rate_options_options_title'	=>	array(
						'name' => __( 'Rate Options', 'xa-shipping-addon' ),
						'type' => 'title',
						'desc' => '',
						'id'   => 'wf_shipping_addon_rate_role_matrix_options_title',
					),	
					'rate_options'	=>	array(
					'type'     => 'rate_options',
					'id'       => 'wf_shipping_addon_rate_role_matrix',
					),			
					'rate_options_options_sectionend'	=>	array(
						'type' => 'sectionend',
						'id'   => 'wf_shipping_addon_rate_role_matrix_options_sectionend'
					),			
				) );
				break;
		}
		return apply_filters( 'woocommerce_get_settings_' . $this->id, $settings, $current_section );	
	}

	public function wf_shipping_addon_output() {
	    global $current_section;
	    $settings = $this->wf_shipping_addon_get_settings( $current_section );
	    WC_Admin_Settings::output_fields( $settings );
	}

	public function wf_shipping_addon_save() {   
	    global $current_section;  
	    $settings = $this->wf_shipping_addon_get_settings( $current_section );
	    WC_Admin_Settings::save_fields( $settings );
	}

	//to add the necessary js scripts and css styles
	/*public function wf_shipping_addon_admin_scripts() {
		wp_enqueue_script( 'wf-settingsAlign-script', plugins_url( '../assests/js/settings.js', __FILE__ ), array( 'jquery' ) );
		wp_enqueue_style( 'wf-timepicker-style', plugins_url( '../assests/css/jquery.timepicker.css', __FILE__ ) );
	}	*/
	public function wf_shipping_addon_scripts() {
		if(is_checkout()&&!is_order_received_page()){
			wp_enqueue_script( 'wf-checkout-script', plugins_url( '../assests/js/checkout.js', __FILE__ ), array( 'jquery' ) );
		}	
	}
	public function generate_label_options_html() {
		include( 'html-label-options.php' );
	}
	public function generate_rate_options_html() {
		include( 'html-rate-options.php' );
	}

	public function wf_shipping_addon_this_screen() {
	    $currentScreen = get_current_screen();
   	 	if( $currentScreen->id == 'woocommerce_page_wc-settings' ){
   	 	
        // add_action( 'admin_enqueue_scripts', array( $this, 'wf_shipping_addon_admin_scripts' ) );
   	 	}
   	 }
	
}
return new Wf_shipping_addon_Settings();