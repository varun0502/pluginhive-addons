<style type="text/css">
    .block1{
	height:30px;
	width:350px;
    }
    
</style>

<?php
	$origin_address_datas = get_option('wf_shipping_addon_label_options');
	if( isset($origin_address_datas[0]['reset']) ){
		update_option('wf_shipping_addon_label_options','');
		$origin_address_datas = '';
		// echo '<div id="message" class="updated inline"><p><strong>Your settings have been reset</strong></p></div>';
	}
?>

<table style="width:66%">
    <tr><td><strong>Custom Text on the label (only for Stamps.com)</strong></td><td > <input class="block1" placeholder="Eg: Order Id: [orderid]" type="text" name="wf_shipping_addon_label_options[0][custom_text]" id="wf_shipping_custom_text" value="<?php echo isset($origin_address_datas[0]['custom_text']) ? $origin_address_datas[0]['custom_text'] : ''; ?>"></td>
</table>
<hr/>
<div><h1 style="margin:0%;">Change Store Address (Origin Address) based on Destination Country</h1></div>
<table id="xa_common_addon_multi_address_serttings_table">
	<?php
	$i=0;
	if( is_array($origin_address_datas) ){
		foreach ($origin_address_datas as $index => $values) {
			if( !empty($values) ) {
				xa_load_address_row($index, $values);
				$i++;
			}
		}
	}
	
    if( $i==0 ){ 
    	$add_row = 1;
    }
    else{
    	$add_row = isset($origin_address_datas[0]['add_new']) ? $origin_address_datas[0]['add_new'] : '';
    }
    while ( intval($add_row) > 0 ) {
		xa_load_address_row($i);
		$add_row--;
    }
	?>
    <hr />
	<tr>
		<td>Add more rows</td>
		<td>
			<select name="wf_shipping_addon_label_options[0][add_new]" style="margin-left: 30px;">
				<option>Choose a number</option><?php
				for ($k=1; $k <= 5 ; $k++) { 
					echo "<option>$k</option>";
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td class="left">
		<?php
		woocommerce_form_field(
	    	"wf_shipping_addon_label_options[0][reset]", array(
	    		'id'		=> "reset",
			    'type'      => "checkbox",
			    'label'     => __("Reset"),
		    )
	    ); ?>	
		</td>
	</tr>
</table>


<?php
function xa_load_address_row($key=0,  $values='' ){
	global $woocommerce;
	$countries_obj   = new WC_Countries();
	$countries   = $countries_obj->get_countries();
	
	$des_country = !empty($values['des_country']) ? $values['des_country'] : '';
	?>
	<style>
		/*Style for tooltip*/
		.xa-tooltip { position: relative;  }
		.xa-tooltip .xa-tooltiptext { visibility: hidden; width: 150px; background-color: black; color: #fff; text-align: center; border-radius: 6px; 
			padding: 5px 0;
			/* Position the tooltip */
			position: absolute; z-index: 1;}
		.xa-tooltip:hover .xa-tooltiptext {visibility: visible;}
		/*End of tooltip styling*/
		</style>
<table style="width:93%">
	<tr><td><strong>Select destination country</strong></td>
		<td>
			<div class="block block1">
			<?php
				woocommerce_form_field(
			    	"wf_shipping_addon_label_options[$key][des_country]", array(
			    		'id'		=> "des_country[]",
					    'type'      => "country",
//					    'label'     => __("Select destination country"),
				    
					    'options'   => $countries,
					   	'default'	=> $des_country,
				    ),
			    	$des_country
			    );
			?>
			</div>
		</td>
	</tr>
	<tr>
				<td><strong> Select Destination State </strong>
				<?php
				echo '<span class="xa-tooltip"><img src="'.site_url("/wp-content/plugins/woocommerce/assets/images/help.png").'" height="16" width="16" /><span class="xa-tooltiptext">Save Country settings first to get the states of that country.</span></span>' 
				?>
				</td>
				
				<td>
				<select class="wc-enhanced-select xa_shipping_common_addon_des_states" multiple="multiple" style="width: 45%;" name="wf_shipping_addon_label_options[<?php echo $key ?>][des_states][]" >
					<?php
						// foreach( $countries as $country_key => $country ) {
							if( !empty($des_country) ) {
								$states_in_country = $countries_obj->get_states($des_country);
								if( ! empty($states_in_country) ) {
									// echo "<optgroup label='".$country."'>";
										foreach( $states_in_country as $state_key => $state_name ) {
											if( ! empty($values['des_states']) && in_array($state_key, $values['des_states']) ) {
												echo "<option value='".$state_key."' selected>".$state_name."</option>";
											}
											else {
												echo "<option value='".$state_key."'>".$state_name."</option>";
											}
										}
									// echo "</optgroup>";
								}
							}
						// }
					?>
					</select>
				</td>
	<tr>
	    <td style="float:left;"><h3 style="margin:0%;">Origin address</h3></td>
	</tr>
	
	
	<tr><td><div class="block"><strong>Name</strong></td><td><div class="block"> <input style="height:30px;width:350px;margin-top:5px;" type="text" name="wf_shipping_addon_label_options[<?php echo $key?>][origin_address_name]" value="<?php echo isset($values['origin_address_name']) ? $values['origin_address_name'] : '' ?>"> </div></td></tr>
	<tr><td><div class="block"><strong>Company</strong></td><td><div class="block"> <input class="block1" type="text" name="wf_shipping_addon_label_options[<?php echo $key?>][origin_address_company]" value="<?php echo isset($values['origin_address_company']) ? $values['origin_address_company'] : '' ?>"></div></td></tr>
	<tr><td><div class="block"><strong>Phone</strong></td><td><div class="block"> <input class="block1" type="text" name="wf_shipping_addon_label_options[<?php echo $key?>][origin_address_phone]" value="<?php echo isset($values['origin_address_phone'])? $values['origin_address_phone'] : '' ?>"></div></td></tr>
	<tr><td><div class="block"><strong>Email</strong></td><td><div class="block"> <input class="block1" type="text" name="wf_shipping_addon_label_options[<?php echo $key?>][origin_address_email]" value="<?php echo isset($values['origin_address_email'])? $values['origin_address_email'] : '' ?>"></div></td></tr>
	<tr><td><div class="block"><strong>Address line 1</strong></td><td><div class="block"> <input class="block1" type="text" name="wf_shipping_addon_label_options[<?php echo $key?>][origin_address_address_1]" value="<?php echo isset($values['origin_address_address_1']) ? $values['origin_address_address_1'] : '' ?>" ></div></td></tr>
	<tr><td><div class="block"><strong>Address line 2</strong></td><td><div class="block"> <input class="block1" type="text" name="wf_shipping_addon_label_options[<?php echo $key?>][origin_address_address_2]" value="<?php echo isset($values['origin_address_address_2']) ? $values['origin_address_address_2'] : '' ?>" ></div></td></tr>
	<tr><td><div class="block"><strong>City</strong></td><td><div class="block"> <input class="block1" type="text" name="wf_shipping_addon_label_options[<?php echo $key?>][origin_address_city]" value="<?php echo isset($values['origin_address_city']) ? $values['origin_address_city'] : '' ?>" ></div></td></tr>
	<tr><td><div class="block"><strong>State</strong></td><td><div class="block"> <input class="block1" type="text" name="wf_shipping_addon_label_options[<?php echo $key?>][origin_address_state]" value="<?php echo isset($values['origin_address_state']) ? $values['origin_address_state'] : ''?>" ></div></td></tr>
	<tr><td><div class="block"><strong>Zip code</strong></td><td><div class="block"> <input class="block1" type="text" name="wf_shipping_addon_label_options[<?php echo $key?>][origin_address_postcode]" value="<?php echo isset($values['origin_address_postcode']) ? $values['origin_address_postcode'] :'' ?>" ></div></td></tr>
	<tr><td><div class="block"><strong>Select Origin Country</strong></td><td><div class="block block1"><?php
			    woocommerce_form_field(
			    	"wf_shipping_addon_label_options[$key][origin_address_country]", array(
			    		'id'		=> 'origin_address_country[]',
					    'type'      => 'country',
//					    'label'     => __('Select a country'),
					    'options'   => $countries,
					   	'default'	=> isset($values['origin_address_country']) ? $values['origin_address_country'] : '',
				    )
			    );?>
		</div></td></tr>
			<hr/>
		
	</tr>
</table><?php
}
?>